#include <iostream>
#include <thread>
#include <cerrno>
#include <cstring>
#include <unistd.h>
#include <pthread.h>
#include <cstdio>

using namespace std;

int tickets = 10000;

void *getTicket(void *args)
{
    (void)args;
    while(true)
    {
        if(tickets > 0)
        {
            usleep(1000);
            printf("%p: %d\n", pthread_self(), tickets);
            tickets--;
        }
        else
        {
            break;
        }
    }
    return nullptr;
}

int main()
{
    pthread_t t1,t2,t3;

    pthread_create(&t1, nullptr, getTicket, nullptr);
    pthread_create(&t2, nullptr, getTicket, nullptr);
    pthread_create(&t3, nullptr, getTicket, nullptr);

    pthread_join(t1, nullptr);
    pthread_join(t2, nullptr);
    pthread_join(t3, nullptr);

    return 0;
}

// void fun()
// {
    // while(true)
    // {
        // cout << "hello new thread" << endl;
        // sleep(1);
    // }
// }
// 
// int main()
// {
    // std::thread t0(fun);
    // std::thread t1(fun);
    // std::thread t2(fun);
    // std::thread t3(fun);
    // std::thread t4(fun);
    // while(true)
    // {
        // cout << "hello main thread" << endl;
        // sleep(1);
    // }
    // return 0;
// }