#include <iostream>
#include <cerrno>
#include <cstring>
#include <unistd.h>
#include <pthread.h>
#include <cstdio>

using namespace std;

//__thread:修饰全局变量，结果是让每一个线程各自拥有一个全局的变量 -- 线程的局部存储
__thread int g_val = 0;

// 线程谁先运行与调度器相关
// 线程一旦异常，都可能导致整个进程整体退出
void *threadRoutine(void *args)
{
    //pthread_detach(pthread_self());//线程分离
    // sleep(3);
    // execl("/bin/ls", "ls", nullptr);//等价于调用exit,全部替换
    while (true)
    {
        cout << (char *)args << " : " << g_val << " &: " << &g_val << endl;
        g_val++;
        sleep(1);
        break;
    }
    pthread_exit((void**)11);
    // int i = 0;
    // int *data = new int[10];
    // while (true)
    // {
    // cout << "新线程: " << (char *)args << " running..." << pthread_self() << endl;
    // sleep(1);
    // data[i] = i;
    // if (i++ == 3)
    // break;
    // int a = 100;
    // a /= 0;
    //}
    // pthread_exit((void *)16);
    //  exit(10);//不要调用exit，exit是终止进程的
    // pthread_cancel(getpid());
    // cout << "pthread_cancel:" << getpid() << endl;
    // cout << "new thread quit..." << endl;
    // return (void*)data;
    // return (void *)10;
}

int main()
{
    pthread_t tid;
    pthread_create(&tid, nullptr, threadRoutine, (void *)"thread 1");
    while (true)
    {
        cout << "main thread" << " : " << g_val << " &: " << &g_val << endl;
        sleep(1);
        break;
    }
    int n = pthread_join(tid, nullptr);
    cout << "n :" << n << " errstring: " << strerror(n) << endl;
    // printf("%lu,%p\n", tid, tid);
    // int count = 0;
    // while (true)
    // {
    // cout << "main线程:"
    //  << " running ... main tid:" << pthread_self() << endl;
    // sleep(1);
    // count++;
    // if (count >= 5)
    // break;
    // }
    // pthread_cancel(tid);
    // cout << "pthread_cancel:" << tid << endl;

    // int *ret = nullptr;
    // pthread_join(tid, (void **)&ret); // 线程等待,默认会阻塞等待新线程退出
    // cout << "mian thread wait done... main quit ... : new thread quit : " << (long long)ret << "\n";
    // sleep(5);
    // for(int i = 0; i < 10; i++)
    // {
    // cout << ret[i] << endl;
    // }

    // 线程在创建并执行的时候，线程也是需要进行等待的，如果主线程不等待，会引起类似于进程的僵尸问题，导致内存泄露
    //  while(true)
    //  {
    //  cout <<"main线程： " << " running ..." << endl;
    //  sleep(1);
    // }

    return 0;
}
