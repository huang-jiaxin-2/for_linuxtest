#pragma once
#include <iostream>
#include <string>
#include <functional>
#include <cstdio>

typedef void *(*fun_t)(void *);

struct ThreadData
{
    void *args_;
    std::string name_;
};

class Thread
{
public:
    Thread(int num, fun_t callback, void *args) : func_(callback)
    {
        char nameBuffer[64];
        snprintf(nameBuffer, sizeof nameBuffer, "Thread-%d", num);
        name_ = nameBuffer;
        tdata_.args_ = args;
        tdata_.name_ = name_;
    }

    // 创建线程
    void start()
    {
        pthread_create(&tid_, nullptr, func_, (void *)&tdata_);
    }

    // 销毁线程所占用的资源
    void join()
    {
        pthread_join(tid_, nullptr);
    }

    // 获取线程名-由于线程ID不太好观察所以用线程名来方便观察
    std::string name()
    {
        return name_;
    }

    ~Thread()
    {
    }

private:
    std::string name_;     // 线程名
    pthread_t tid_;        // 存储线程ID
    ThreadData tdata_;     // 存储传递给线程的数据
    fun_t func_;          // 回调函数
};

