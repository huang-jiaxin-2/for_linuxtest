#pragma once

#include <iostream>
#include <string>
#include <ctime>
#include <cstdio>
#include <cstdarg>

// 日志级别
#define DEBUG 0
#define NORMAL 1
#define WARNING 2
#define ERROR 3
#define FATAL 4

const char *gLevelMap[] = {
    "DEBUG",
    "NORMAL",
    "WARNING",
    "ERROR",
    "FATAL"};

#define LOGFILE "./threadpool.log" // 日志文件路径

//日志等级、时间、日志内容、支持用户自定义
void logMessage(int level, const char *format, ...)
{
    // 调试小技巧
#ifndef DEBUG_SHOW
    if (level == DEBUG)
        return;
#endif
    char stdBuffer[1024]; // 标准部分
    time_t timestamp = time(nullptr);
    snprintf(stdBuffer, sizeof stdBuffer, "[%s][%ld]", gLevelMap[level], timestamp);

    char logBuffer[1024]; // 自定义部分
    va_list args;//va_list：char类型指针
    va_start(args, format);
    vsnprintf(logBuffer, sizeof logBuffer, format, args);
    va_end(args);

    // 写入文件中
    //  FILE* fp = fopen(LOGFILE, "a");
    //  fprintf(fp, "%s%s\n", stdBuffer, logBuffer);
    //  fclose(fp);

    // 打印输出
    printf("%s%s\n", stdBuffer, logBuffer);
}