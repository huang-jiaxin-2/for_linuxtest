#include "threadPool.hpp"
#include "Task.hpp"
#include "Log.hpp"
#include <ctime>
#include <cstdlib>
#include <unistd.h>
#include <iostream>

int main()
{
    srand((unsigned long)time(nullptr) ^ getpid());
    ThreadPool<Task> *tp = new ThreadPool<Task>();
    tp->run();
    while (true)
    {
        int x = rand() % 100 + 1;
        usleep(8000);
        int y = rand() % 300 + 1;
        Task t(x, y, [](int x, int y) -> int
               { return x + y; }); // lambda表达式

        logMessage(DEBUG, "制作任务完成: %d+%d=?", x, y);
        tp->pushTask(t);
        sleep(1);
    }
    delete tp;
    return 0;
}