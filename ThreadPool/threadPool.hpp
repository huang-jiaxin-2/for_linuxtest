#pragma once

#include <iostream>
#include <queue>
#include <unistd.h>
#include "thread.hpp"
#include "Log.hpp"
#include "lockGuard.hpp"

const int default_num = 5;

//线程池——本质是生产者消费者模型
template <class T>
class ThreadPool
{
public:
    pthread_mutex_t *getMutex()
    {
        return &_lock;
    }

    bool isEmpty()
    {
        return _task_queue.empty();
    }

    void waitCond()
    {
        pthread_cond_wait(&_cond, &_lock);
    }

    //获取任务
    T getTask()
    {
        T t = _task_queue.front();
        _task_queue.pop();
        return t;
    }

    ThreadPool(int thread_num = default_num) : _num(thread_num)
    {
        pthread_mutex_init(&_lock, nullptr);
        pthread_cond_init(&_cond, nullptr);
        for (int i = 1; i <= _num; i++)
        {
            _threads.push_back(new Thread(i, routine, this));
        }
    }
    void run()
    {
        for (auto &it : _threads)
        {
            it->start();
            logMessage(NORMAL, "%s %s", it->name().c_str(), "启动成功");
        }
    }

    // 消费者——处理任务
    static void *routine(void *args)
    {
        ThreadData *td = (ThreadData *)args;
        ThreadPool<T> *tp = (ThreadPool<T> *)td->args_;
        while (true)
        {
            T task;
            // 获取任务
            {
                lockGuard lockguard(tp->getMutex());
                while (tp->isEmpty())
                    tp->waitCond();

                task = tp->getTask();
            }
            // 处理任务
            task(td->name_);
        }
    }

    // 生产者——生产任务
    void pushTask(const T &task)
    {
        lockGuard lockguard(&_lock); // 加锁保护
        _task_queue.push(task);
        pthread_cond_signal(&_cond); // 唤醒线程处理任务
    }

    ~ThreadPool()
    {
        for (auto &it : _threads)
        {
            it->join(); // 等待资源释放
            delete it;
        }
        pthread_mutex_destroy(&_lock);
        pthread_cond_destroy(&_cond);
    }

private:
    std::vector<Thread *> _threads;
    int _num; // 线程个数
    std::queue<T> _task_queue;
    pthread_mutex_t _lock; // 互斥锁
    pthread_cond_t _cond;
};