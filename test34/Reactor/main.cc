#include "TcpServer.hpp"
#include <memory>

static Response calculator(const Request &req)
{
    Response resp(0, 0);
    switch (req.op_)
    {
    case '+':
        resp.result_ = req.x_ + req.y_;
        break;
    case '-':
        resp.result_ = req.x_ - req.y_;
        break;
    case '*':
        resp.result_ = req.x_ * req.y_;
        break;
    case '/':
        if (0 == req.y_)
            resp.code_ = 1;
        else
            resp.result_ = req.x_ / req.y_;
        break;
    case '%':
        if (0 == req.y_)
            resp.code_ = 2;
        else
            resp.result_ = req.x_ % req.y_;
        break;
    default:
        resp.code_ = 3;
        break;
    }
    return resp;
}


void NetCal(Connection *conn, std::string &request)
{
    logMessage(DEBUG, "NetCal been called, get request: %s", request.c_str());

    //反序列化
    Request req;
    if(!req.Deserialized(request)) return;
    //处理业务
    Response resp = calculator(req);
    //序列化，构建应答
    std::string sendstr = resp.Serialize();
    sendstr = Encode(sendstr);
    //交给服务器
    conn->_outbuffer += sendstr;
    conn->_tsvr->EnableReadWrite(conn, true, true);

}

int main()
{
    std::unique_ptr<TcpServer> svr(new TcpServer());
    svr->Dispather(NetCal);
    
    return 0;
}