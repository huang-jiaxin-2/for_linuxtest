#pragma once

#include <iostream>
#include <functional>
#include <string>
#include <vector>
#include <cassert>
#include <ctime>
#include <unordered_map>
#include "Log.hpp"
#include "Sock.hpp"
#include "Epoll.hpp"
#include "Protocol.hpp"

class TcpServer;
class Connection;

using func_t = std::function<void(Connection *)>;
using callback_t = std::function<void(Connection *, std::string &request)>;

class Connection
{
public:
    Connection(int sock = -1) : _sock(sock), _tsvr(nullptr)
    {
    }
    void SetCallBack(func_t recv_cb, func_t send_cb, func_t except_cb)
    {
        _recv_cb = recv_cb;
        _send_cb = send_cb;
        _except_cb = except_cb;
    }
    ~Connection()
    {
    }

public:
    // 进行IO的文件描述符
    int _sock;
    // 回调方法，对_sock进行特定的读写对应的方法
    func_t _recv_cb;
    func_t _send_cb;
    func_t _except_cb;
    // 接收缓冲区和发送缓冲区
    std::string _inbuffer;
    std::string _outbuffer;
    // TcpSerrver的回值指针
    TcpServer *_tsvr;
    //时间戳
    uint64_t _lasttimestamp;
};

class TcpServer
{
    const static int gport = 8080;
    const static int gnum = 128;

public:
    TcpServer(int port = gport) : _port(port), _revs_num(gnum)
    {
        // 1.创建listensock
        _listensock = Sock::Socket();
        Sock::Bind(_listensock, _port);
        Sock::Listen(_listensock);

        // 2.创建多路转接对象
        _poll.CreateEpoll();

        // 3.添加listensock到服务器中
        AddConnection(_listensock, std::bind(&TcpServer::Accepter, this, std::placeholders::_1), nullptr, nullptr);

        // 4.构建一个获取就绪事件的缓冲区
        _revs = new struct epoll_event[_revs_num];
    }

    void AddConnection(int sock, func_t recv_cb, func_t send_cb, func_t except_cb)
    {
        Sock::SetNonBlock(sock);
        // 1.创建conn对象，封装sock
        Connection *conn = new Connection(sock);
        conn->SetCallBack(recv_cb, send_cb, except_cb);
        conn->_tsvr = this;
        // 2.添加sock到epoll中
        _poll.AddSockToEpoll(sock, EPOLLIN | EPOLLET);
        // 3.将对应的Connection*对象指针添加到Connection映射表中
        _connections.insert(std::make_pair(sock, conn));
    }

    void Accepter(Connection *conn)
    {
        // logMessage(DEBUG, "Accepter been called");
        while (true)
        {

            std::string clientip;
            uint16_t clientport;
            int accept_errno = 0;
            int sock = Sock::Accept(conn->_sock, &clientip, &clientport, &accept_errno);
            if (sock < 0)
            {
                if (accept_errno == EAGAIN || accept_errno == EWOULDBLOCK)
                {
                    break;
                }
                else if (accept_errno == EINTR)
                {
                    continue;
                }
                else
                {
                    logMessage(WARNING, "accept error, %d : %s", accept_errno, strerror(accept_errno));
                    break;
                }
            }
            if (sock >= 0)
            {
                AddConnection(sock, std::bind(&TcpServer::Recver, this, std::placeholders::_1),
                              std::bind(&TcpServer::Sender, this, std::placeholders::_1),
                              std::bind(&TcpServer::Excepter, this, std::placeholders::_1));
                logMessage(DEBUG, "accept client %s : %d success, add to epoll&&TcpServer success, sock: %d", clientip.c_str(), clientport, sock);
            }
        }
    }

    void EnableReadWrite(Connection *conn, bool readable, bool writeable)
    {
        uint32_t events = ((readable ? EPOLLIN : 0) | (writeable ? EPOLLOUT : 0));
        bool res = _poll.CtrlEpoll(conn->_sock, events);
        assert(res);
    }

    void Recver(Connection *conn)
    {
        // logMessage(DEBUG, "Recver event exists, Recver() been called");
        const int num = 1024;
        bool err = false;
        while (true)
        {
            char buffer[num];
            ssize_t n = recv(conn->_sock, buffer, sizeof(buffer) - 1, 0);
            if (n < 0)
            {
                if (errno == EAGAIN || errno == EWOULDBLOCK)
                    break;
                else if (errno == EINTR)
                    continue;
                else
                {
                    logMessage(ERROR, "recv error, %d : %s", errno, strerror(errno));
                    conn->_except_cb(conn);
                    err = true;
                    break;
                }
            }
            else if(n == 0)
            {
                logMessage(DEBUG, "client[%d] quit, server close [%d]", conn->_sock, conn->_sock);
                conn->_except_cb(conn);
                err = true;
                break;
            }
            else
            {
                // 读取成功
                buffer[n] = 0;
                conn->_inbuffer += buffer;
            }
        }
        logMessage(DEBUG, "conn->_inbuffer[sock: %d]: %s", conn->_sock, conn->_inbuffer.c_str());
        if (!err)
        {
            std::vector<std::string> messages;
            SpliteMessage(conn->_inbuffer, &messages);
            for (auto &msg : messages)
            {
                _cb(conn, msg);
            }
        }
    }

    void Sender(Connection *conn)
    {
        while (true)
        {
            ssize_t n = send(conn->_sock, conn->_outbuffer.c_str(), conn->_outbuffer.size(), 0);
            if (n > 0)
            {
                conn->_outbuffer.erase(0, n);
                if (conn->_outbuffer.empty())
                    break;
            }
            else
            {
                if (errno == EAGAIN || errno == EWOULDBLOCK)
                    break;
                else if (errno == EINTR)
                    continue;
                else
                {
                    logMessage(ERROR, "send error, %d : %s", errno, strerror(errno));
                    conn->_except_cb(conn);
                    break;
                }
            }
        }
        if(conn->_outbuffer.empty()) EnableReadWrite(conn, true, false);
        else EnableReadWrite(conn, true, true);
    }

    void Excepter(Connection *conn)
    {
        if(!IsConnectionExists(conn->_sock)) return;
        //从epoll中移除
        bool res = _poll.DelEpoll(conn->_sock);
        assert(res);
        //从unorderer_map中移除
        _connections.erase(conn->_sock);
        //close(sock)
        close(conn->_sock);
        //delete
        delete conn;
        logMessage(DEBUG, "Excepter 回收完毕，所有的异常情况");
    }

    void LoopOnce()
    {
        int n = _poll.WaitEpoll(_revs, _revs_num);
        for (int i = 0; i < n; i++)
        {
            int sock = _revs[i].data.fd;
            uint32_t revents = _revs[i].events;

            //将所有的异常，全部交给read或者write来统一处理
            if(revents & EPOLLERR) revents |= (EPOLLIN | EPOLLOUT);
            if(revents & EPOLLHUP) revents |= (EPOLLIN | EPOLLOUT);

            if (revents & EPOLLIN)
            {
                if (IsConnectionExists(sock) && _connections[sock]->_recv_cb != nullptr)
                {
                    _connections[sock]->_recv_cb(_connections[sock]);
                }
            }
            if (revents & EPOLLOUT)
            {
                if (IsConnectionExists(sock) && _connections[sock]->_send_cb != nullptr)
                {
                    _connections[sock]->_send_cb(_connections[sock]);
                }
            }
        }
    }

    bool IsConnectionExists(int sock)
    {
        auto iter = _connections.find(sock);
        if (iter == _connections.end())
            return false;
        else
            return true;
    }

    void ConnectAliveCheck()
    {

    }

    // 根据就绪的事件，进行特定事件的派发
    void Dispather(callback_t cb)
    {
        _cb = cb;
        while (true)
        {
            ConnectAliveCheck();
            LoopOnce();
        }
    }

    ~TcpServer()
    {
        if (_listensock >= 0)
            close(_listensock);
        if (_revs)
            delete[] _revs;
    }

private:
    int _listensock;
    int _port;
    Epoll _poll;
    std::unordered_map<int, Connection *> _connections;
    struct epoll_event *_revs;
    int _revs_num;

    // 上层的业务处理
    callback_t _cb;
};