#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int main()
{
  printf("hello printf\n");
  fprintf(stdout, "hello fprintf\n");
  const char* s1 = "hello fputs\n";
  fputs(s1, stdout);

  const char* s2 = "hello write\n";
  write(1, s2, strlen(s2));
  fork();


//  if(argc != 2)
//  {
//    return 2;
//  }
//  //int fd = open("log.txt", O_WRONLY | O_CREAT | O_TRUNC);//输出
//  int fd = open("log.txt", O_WRONLY | O_CREAT | O_APPEND);//追加重定向
//  if(fd < 0)
//  {
//    perror("open");
//    return 1;
//  }
//
//  dup2(fd, 1);
//  fprintf(stdout, "%s\n", argv[1]);
//

//
// // close(1);
// // //int fd = open("log.txt", O_RDONLY);
// // int fd = open("log.txt", O_WRONLY | O_APPEND | O_CREAT);
// // if(fd < 0)
// // {
// //   perror("open");
// //   return 1;
// // }
 // fprintf(stdout, "you are right, success\n");

  //printf("fd: %d\n", fd);

  //char buffer[64];
  //fgets(buffer, sizeof buffer, stdin);
  //printf("%s\n", buffer);
  //close(1);//fd的分配原则:最小的没有被占用的文件描述符

  //int fd = open("log.txt", O_WRONLY | O_CREAT | O_TRUNC, 0666);
  //if(fd < 0)
  //{
  //  perror("open");
  //  return 1;
  //}

  //printf("fd: %d\n", fd);
  //printf("fd: %d\n", fd);
  //printf("fd: %d\n", fd);
  //printf("fd: %d\n", fd);
  //fprintf(stdout, "hello fprintf\n");
  //const char* s = "hello fwrite\n";
  //fwrite(s, strlen(s), 1, stdout);
  //fflush(stdout);

  //close(fd);
  return 0;
}
