#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

int g_unval;//未初始化数据
int g_val = 100;//初始化数据

int main(int argc, char* argv[], char* env[])
{
  const char* str =  "helloworld";
  static int a = 10;
  printf("code addr: %p\n", main);//正文代码
  printf("init global addr: %p\n", &g_val);//初始化数据
  printf("uninit global add:%p\n", &g_unval);//未初始化数据
  
  printf("a stack addr:%p\n", &a);
  printf("read only string addr:%p\n", str);
//  char* head_memory = (char*)malloc(10);
//  char* head_memory1 = (char*)malloc(10);
//  char* head_memory2 = (char*)malloc(10);
//  char* head_memory3 = (char*)malloc(10);
//  printf("heap addr:%p\n", head_memory);//堆区
//  printf("heap addr:%p\n", head_memory1);//堆区
//  printf("heap addr:%p\n", head_memory2);//堆区
//  printf("heap addr:%p\n", head_memory3);//堆区
//
//  printf("stack addr:%p\n", &head_memory);//栈区
//  printf("stack addr:%p\n", &head_memory1);//栈区
//  printf("stack addr:%p\n", &head_memory2);//栈区
//  printf("stack addr:%p\n", &head_memory3);//栈区
//
//  for(int i = 0; i < argc; i++)
//  {
//    printf("argv[%d]:%p\n", i, argv[i]);//命令行参数
//  }
//  for(int i = 0; env[i]; i++)
//  {
//    printf("env[%d]:%p\n", i, env[i]);//环境变量
//  }
}
