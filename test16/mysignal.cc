#include <iostream>
#include <unistd.h>
#include <signal.h>
#include <cassert>

using namespace std;

static void showPending(sigset_t &pending)
{
    for (int i = 1; i <= 31; i++)
    {
        if (sigismember(&pending, i))
            cout << 1;
        else
            cout << 0;
    }
    cout << endl;
}

void catchSig(int signum)
{
    cout << "捕获信号: " << signum << endl;
}

static void blockSig(int sig)
{
    sigset_t bset;
    sigemptyset(&bset);
    sigaddset(&bset, sig);
    int n = sigprocmask(SIG_BLOCK, &bset, nullptr);
    assert(n == 0);
    (void)n;
}
//9号信号不会被屏蔽或阻塞
int main()
{
    for(int sig = 1; sig <= 31; sig++)
    {
        blockSig(sig);
    }
    sigset_t pending;
    while(true)
    {
        sigpending(&pending);
        showPending(pending);
        sleep(1);
    }
    return 0;
}

// static void showPending(sigset_t &pending)
// {
    // for (int i = 1; i <= 31; i++)
    // {
        // if (sigismember(&pending, i))
            // cout << 1;
        // else
            // cout << 0;
    // }
    // cout << endl;
// }
// 
// void catchSig(int signum)
// {
    // cout << "捕获信号: " << signum << endl;
// }
// 
// int main()
// {
    //捕捉2号信号
    // signal(2, catchSig);
    //1.定义信号集对象
    // sigset_t bset, obset, pending;
    //2.初始化
    // sigemptyset(&bset);
    // sigemptyset(&obset);
    // sigemptyset(&pending);
    //3.添加要进行屏蔽的信号--2号信号
    // sigaddset(&bset, 2);
    //4.设置set到内核中对应的进程内部
    // int n = sigprocmask(SIG_BLOCK, &bset, &obset);
    // assert(n == 0);
    // (void)n; // 在linux中的g++下，如果变量定义了没有被使用是会告警(release)
    // cout << "block 2号信号成功... pid: " << getpid() << endl;
    //5.重复打印当前进程的pending信号集
    // int count = 0;
    // while (true)
    // {
        //获取当前进程的pending的信号集
        // sigpending(&pending);
        //显示pending信号集中没有被递达的信号
        // showPending(pending);
        // sleep(1);
        // count++;
        // if (count == 20)
        // {
            //现象是进程被杀死了
            //原因是恢复对2号信号的block的时候，确实会递达
            //但是2号信号的作用是终止进程
            //所以要对2号信号进行捕捉
            // cout << "解除对2号信号的block" << endl;
            // int n = sigprocmask(SIG_SETMASK, &obset, nullptr);
            // assert(n == 0);
            // (void)n; // 在linux中的g++下，如果变量定义了没有被使用是会告警(release)
        // }
    // }
// 
    // return 0;
// }

// 所有信号都进行自定义捕捉依然是可以被杀掉的--9号信号做了特殊处理
//  void catchSig(int signum)
//  {
//  cout << "获取了一个信号: " << signum << endl;
// }
//
// int main()
// {
// for(int i = 0; i < 31; i++)
// {
// signal(i, catchSig);
// }
//
// while(true) sleep(1);
// signal(2,SIG_IGN);//SIG_IGN--忽略 1
// signal(2,SIG_DFL);//SIG_DFL--默认 0
// return 0;
// }