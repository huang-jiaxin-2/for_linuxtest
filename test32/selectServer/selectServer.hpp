#ifndef __SELECT_SVR_H__
#define __SELECT_SVR_H__

#include <iostream>
#include <string>
#include <sys/time.h>
#include <sys/select.h>
#include "Log.hpp"
#include "Sock.hpp"

#define BITS 8
#define NUM (sizeof(fd_set) * BITS)
#define FD_NONE -1

using namespace std;

class SelectServer
{
public:
    SelectServer(const int16_t port = 8080) : _port(port)
    {
        _listensock = Sock::Socket();
        Sock::Bind(_listensock, _port);
        Sock::Listen(_listensock);
        logMessage(DEBUG, "create base socket success");
        for (int i = 0; i < NUM; i++)
            _fd_array[i] = FD_NONE;

        _fd_array[0] = _listensock;
    }

    void Start()
    {
        while (true)
        {
            // struct timeval timeout = {5, 0};
            // FD_SET(_listensock, &rfds);

            DebugPrint();

            fd_set rfds;
            FD_ZERO(&rfds);
            int maxfd = _listensock;
            for (int i = 0; i < NUM; i++)
            {
                if (_fd_array[i] == FD_NONE)
                    continue;
                FD_SET(_fd_array[i], &rfds);
                if (maxfd < _fd_array[i])
                    maxfd = _fd_array[i];
            }

            int n = select(maxfd + 1, &rfds, nullptr, nullptr, nullptr);
            // int n = select(listensock + 1, &rfds, nullptr, nullptr, &timeout);

            switch (n)
            {
            case 0:
                logMessage(DEBUG, "time out...");
                break;
            case -1:
                logMessage(WARNING, "select error: %d : %s", errno, strerror(errno));
                break;
            default:
                logMessage(DEBUG, "get a new link event...");
                HandlerEvent(rfds);
                break;
            }
        }
    }

    ~SelectServer()
    {
        if (_listensock >= 0)
            close(_listensock);
    }

private:
    void HandlerEvent(const fd_set &rfds)
    {
        for (int i = 0; i < NUM; i++)
        {
            // 1.去掉不合法的fd
            if (_fd_array[i] == FD_NONE)
                continue;
            // 2.合法判断是否就绪
            if (FD_ISSET(_fd_array[i], &rfds))
            {
                // 指定fd,读事件就绪
                if (_fd_array[i] == _listensock)
                {
                    Accepter();
                }
                else
                {
                    Recver(i);
                }
            }
        }
    }

    void Accepter()
    {
        string clientip;
        uint16_t clientport = 0;
        int sock = Sock::Accept(_listensock, &clientip, &clientport);
        if (sock < 0)
        {
            logMessage(WARNING, "accept error");
            return;
        }
        logMessage(DEBUG, "get a new link success : [%s:%d] : %d", clientip.c_str(), clientport, sock);
        int pos = 0;
        for (; pos < NUM; pos++)
        {
            if (_fd_array[pos] == FD_NONE)
                break;
        }
        if (pos == NUM)
        {
            logMessage(WARNING, "%S:%d", "select server already full, close: %d", sock);
            close(sock);
        }
        else
        {
            _fd_array[pos] = sock;
        }
    }

    void Recver(int pos)
    {
        logMessage(DEBUG, "message in, get IO event: %d", _fd_array[pos]);
        char buffer[1024];
        int n = recv(_fd_array[pos], buffer, sizeof(buffer) - 1, 0);
        if (n > 0)
        {
            buffer[n] = 0;
            logMessage(DEBUG, "client[%d]# %s", _fd_array[pos], buffer);
        }
        else if (n == 0)
        {
            logMessage(DEBUG, "client[%d] quit, me too...", _fd_array[pos]);
            // 关闭不需要的fd
            close(_fd_array[pos]);
            // 不要让select关心当前的fd了
            _fd_array[pos] = FD_NONE;
        }
        else
        {
            logMessage(WARNING, "%d sock recv error, %d : %s", _fd_array[pos], errno, strerror(errno));
            // 关闭不需要的fd
            close(_fd_array[pos]);
            // 不要让select关心当前的fd了
            _fd_array[pos] = FD_NONE;
        }
    }

    void DebugPrint()
    {
        cout << "_fd_array[]: ";
        for (int i = 0; i < NUM; i++)
        {
            if (_fd_array[i] == FD_NONE)
                continue;
            cout << _fd_array[i] << " ";
        }
        cout << endl;
    }

private:
    uint16_t _port;
    int _listensock;
    int _fd_array[1024];
};

#endif