#include "selectServer.hpp"
#include <memory>

int main()
{
    // std::cout << sizeof(fd_set) * 8 << std::endl;

    std::unique_ptr<SelectServer> svr(new SelectServer());
    svr->Start();
    return 0;
}