#include <iostream>
#include <cstring>
#include <cerrno>
#include <ctime>
#include <cassert>
#include <unistd.h>
#include <fcntl.h>
#include <sys/time.h>

int main()
{
    while(true)
    {
        std::cout << "time: " << (unsigned long)time(nullptr) << std::endl;
        struct timeval currtime = {0, 0};
        int n = gettimeofday(&currtime, nullptr);
        assert(n == 0);
        (void)n;
        std::cout << "gettimeofday: " << currtime.tv_sec << "   " << currtime.tv_usec << std::endl;
        sleep(1);
    }
    return 0;
}

// bool SetNonBlock(int fd)
// {
    // int fl = fcntl(fd, F_GETFL); // 在底层获取当前fd对应的文件读写标注位
    // if (fl < 0)
    // {
        // return false;
    // }
    // fcntl(fd, F_SETFL, fl | O_NONBLOCK); // 设置非阻塞
    // return true;
// }
// 
// int main()
// {
    // SetNonBlock(0); // 只要设置一次,后续就都是非阻塞
    // char buffer[1024];
    // while (true)
    // {
        // sleep(1);
        // errno = 0;
        // ssize_t s = read(0, buffer, sizeof(buffer));
        // if (s > 0)
        // {
            // buffer[s] = 0;
            // std::cout << "echo# " << buffer << " errno[---]: " << errno << " errstring: " << strerror(errno) << std::endl;
        // }
        // else
        // {
            // std::cout << "read \"error\"" << " errno: " << errno << " errstring: " << strerror(errno) << std::endl;
            // if (errno == EWOULDBLOCK || errno == EAGAIN)
            // {
                // std::cout << "当前0号fd数据没有就绪，请下一次再来试一试吧" << std::endl;
                // continue;
            // }
            // else if (errno == EINTR)
            // {
                // std::cout << "当前IO可能被信号中断，在试一试吧" << std::endl;
                // continue;
            // }
            // else
            // {
            // }
        // }
    // }
    // return 0;
// }