#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

int main()
{
  //c语言接口
  printf("I am printf\n");
  fprintf(stdout, "I am fprintf\n");
  const char *s = "I am s\n";
  fputs(s, stdout);

  //操作系统接口
  const char *s1 = "I am s1\n";
  write(1, s1, strlen(s1));

  fork();
  return 0;
}



//int main()
//{
//  int fd = open("log.txt", O_WRONLY|O_CREAT|O_APPEND, 0666);
//  if(fd < 0)
//  {
//    perror("open");
//    return 1;
//  }
//  dup2(fd, 1);
//  fprintf(stdout, "hello dup2\n");
//
//  close(fd);
//  return 0;
//}


//int main()
//{
//  //close(0);
//  //close(2);
//  close(1);
//  int fd = open("log.txt", O_WRONLY|O_CREAT|O_APPEND);
//  if(fd < 0)
//  {
//    perror("open");
//    return 1;
//  }
//
//  fprintf(stdout, "hello hjx\n");
//
//  //printf("fd: %d\n", fd); 
//  //char buffer[64];
//  //fgets(buffer, sizeof buffer, stdin);
// // printf("%s\n", buffer);
// // printf("fd: %d\n", fd); 
// // printf("fd: %d\n", fd); 
// // printf("fd: %d\n", fd); 
// // printf("fd: %d\n", fd); 
// // printf("fd: %d\n", fd);
//
//  fflush(stdout);
//  close(fd);
//  return 0;
//}



//int main()
//{
//  printf("stdin:%d\n", stdin->_fileno);
//  printf("stdout:%d\n", stdout->_fileno);
//  printf("stderr:%d\n", stderr->_fileno);
//
//  //char buf[64];
//  //ssize_t s = read(0, buf, sizeof(buf));
//  //if(s > 0)
//  //{
//  //  buf[s] = '\0';//在读取到的有效数据的末尾添加\0
//  //  printf("%s\n",buf);
//  //}
//  //fprintf(stdout, "hello stdout\n");
//  //const char *s1 = "hello 1\n";
//  //write(1, s1, strlen(s1));
//  return 0;
//}


//int main()
//{
//  int fd1 = open("log1.txt", O_WRONLY|O_CREAT);
//  int fd2 = open("log2.txt", O_WRONLY|O_CREAT);
//  int fd3 = open("log3.txt", O_WRONLY|O_CREAT);
//  int fd4 = open("log4.txt", O_WRONLY|O_CREAT);
//
//  printf("open success, fd1:%d\n", fd1);
//  printf("open success, fd2:%d\n", fd2);
//  printf("open success, fd3:%d\n", fd3);
//  printf("open success, fd4:%d\n", fd4);
//  
//  //char buffer[64];
//  //memset(buffer, '\0', sizeof(buffer));
//  //read(fd, buffer, sizeof(buffer));
//  //printf("%s\n", buffer);
//  ////const char *s1 = "hello write\n";
//  //write(fd, s1, strlen(s1));
//    
//  close(fd1);
//  close(fd2);
//  close(fd3);
//  close(fd4);
//  return 0;
//}
//
//


//用不重复的位就可以标识一个状态
//#define ONE 0x1 //0000 0001
//#define TWO 0x2 //0000 0010
//#define THREE 0x4 //0000 0100
//
//
//void print(int flags)
//{
//  //&运算:有0则为0,全1才为1
//  if(flags & ONE) printf("I am ONE\n");
//  if(flags & TWO) printf("I am TWO\n");
//  if(flags & THREE) printf("I am THREE\n");
//}
//
//int main()
//{
//  print(ONE);
//  printf("--------------------------------\n");
//  print(TWO);
//  printf("--------------------------------\n");
//  print(THREE);
//  printf("--------------------------------\n");
//  print(ONE | TWO | THREE);//0000 0001 | 0000 0010 | 0000 0100 = 0000 0111
//  printf("--------------------------------\n");
//  print(ONE | TWO);//0000 0001 | 0000 0010 = 0000 0011
//  printf("--------------------------------\n");
//  print(ONE | THREE);//0000 0001 | 0000 0100 = 0000 0101
//  printf("--------------------------------\n");
//  return 0;
//}

//int main(int argc, char *argv[])//命令行参数
//{
//  if(argc != 2)
//  {
//    printf("argv error");
//    return 1;
//  }
//  FILE *fp = fopen(argv[1], "r");
//  if(fp == NULL)
//  {
//    perror("fopen");
//    return 2;
//  }
//  
//  //const char *s1 = "hello Linux\n";
//  //fwrite(s1, strlen(s1), 1, fp);
//  char line[64];
//  while(fgets(line, sizeof(line), fp) != NULL)
//  {
//    fprintf(stdout, "%s", line);
//  }
//
//  fclose(fp);
//  return 0;
//}
//
