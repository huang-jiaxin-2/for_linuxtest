#pragma once

#include <iostream>
#include <string>
#include <functional>
#include "log.hpp"

typedef std::function<int(int, int)> func_t;

int myAdd(int x, int y)
{
    return x + y;
}
int mySub(int x, int y)
{
    return x - y;
}
int myMul(int x, int y)
{
    return x * y;
}
int myDiv(int x, int y)
{
    return x / y;
}

class Task
{
public:
    Task() {}
    Task(int x, int y, func_t func) : x_(x), y_(y), func_(func)
    {
    }
    void operator()(const std::string &name)
    {
        //std::cout << "线程 " << name << " 处理完成，结果是: " << x_ << "+" << y_ << "=" << func_(x_, y_) << std::endl;
        logMessage(WARNING, "线程%s处理完成: %d+%d=%d | %s | %d", name.c_str(), x_, y_, func_(x_, y_), __FILE__, __LINE__);
    }
public:
    int x_;
    int y_;
    func_t func_;
};