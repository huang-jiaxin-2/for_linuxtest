#include <iostream>
#include <vector>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>

int main()
{
  printf("当前进程的开始代码\n");

 // execl("/usr/bin/ls","ls",NULL);
  //execl("/usr/bin/ls","ls","-l",NULL);

  execl("/usr/bin/ls","ls","--color=auto","-l",NULL);
  //execl("/usr/bin/top","top",NULL);
  printf("当前进程的结束代码\n");
  return 0;
}



//
//typedef void (*handler_t)();//函数指针类型
//
//std::vector<handler_t> handlers;//函数指针数组
//
//void fun_one()
//{
//  printf("这是一个临时任务1\n");
//}
//
//void fun_two()
//{
////  printf("这是一个临时任务2\n");
//}
//
//void Load()
//{
//  handlers.push_back(fun_one);
//  handlers.push_back(fun_two);
//}
//
//int main()
//{
//  pid_t id = fork();
//  if(id == 0)
//  {
//    //子进程
//   int cnt = 5;
//   while(cnt)
//   {
//     printf("我是子进程：%d\n",cnt--);
//     sleep(1);
//   }
//   exit(11);//仅测试用
//  }
//  else
//  {
//    int quit = 0;
//    while(!quit)
//    {
//      int status = 0;
//      pid_t res = waitpid(-1, &status, WNOHANG);
//      if(res > 0)
//      {
//        //等待成功 && 子进程退出
//        printf("等待子进程退出成功,退出码: %d\n",WEXITSTATUS(status));
//        quit = 1;
//      }
//      else if(res == 0)
//      {
//        //等待成功 && 但子进程并未推出
//        printf("子进程还在运行中,暂时还没有退出,父进程可以再等一等,处理一下其他事情\n");
//        if(handlers.empty()) Load();
//
//        for(auto iter : handlers)
//        {
//          iter();
//        }
//      }
//      else{
//        //等待失败
//        printf("wait失败\n");
//        quit = 1;
//      }
//      sleep(1);
//    }
//
//
//
//    ////父进程
//    //int status = 0;
//    ////只有子进程退出的时候，父进程才会调waitpid函数进行返回
//    ////waitpid/wait 可以在目前的情况下，让进程退出具有一定的顺序性
//    ////将来可以让父进程进行更多的收尾工作，
//    //pid_t result  = waitpid(id, &status, 0);
//    //if(result > 0)
//    //{
//    //   // printf("父进程等待成功，退出码: %d, 退出信号: %d\n", (status>>8)&0xFF, status & 0x7F);
//    //   if(WIFEXITED(status))
//    //   {
//    //     printf("子进程执行完毕,子进程的退出码: %d\n", WEXITSTATUS(status));
//    //   }
//    //   else{
//    //    printf("子进程异常退出; %d\n", WIFEXITED(status));
//    //   }
//    //}
//    
//  }
//}
