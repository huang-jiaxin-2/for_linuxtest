#ifndef _Ring_QUEUE_HPP_
#define _Ring_QUEUE_HPP_

#include <iostream>
#include <vector>
#include <pthread.h>
#include "sem.hpp"

const int g_dafault_num = 5;

template <class T>
class RingQueue
{
public:
    RingQueue(int default_num = g_dafault_num)
        : _ring_queue(default_num),
          _num(default_num),
          c_step(0),
          p_step(0),
          _space_sem(default_num),
          _data_sem(0)
    {
        pthread_mutex_init(&clock, nullptr);
        pthread_mutex_init(&plock, nullptr);
    }
    ~RingQueue()
    {
        pthread_mutex_destroy(&clock);
        pthread_mutex_destroy(&plock);
    }

    // 生产者：空间资源
    void push(const T &in)
    {
        _space_sem.p();
        pthread_mutex_lock(&clock); // 本质在保护下标
        _ring_queue[p_step++] = in;
        p_step %= _num;
        pthread_mutex_unlock(&clock);
        _data_sem.v();
    }

    // 消费者：数据资源
    void pop(T *out)
    {
        _data_sem.p();
        pthread_mutex_lock(&plock);
        *out = _ring_queue[c_step++];
        c_step %= _num;
        pthread_mutex_unlock(&plock);
        _space_sem.v();
    }

    // void debug()
    // {
    //     std::cerr << "size: " << _ring_queue.size() << " num: " << _num << std::endl;
    // }

private:
    std::vector<T> _ring_queue; // 数组充当环形队列结构
    int _num;                   // 环形队列的大小
    int c_step;                 // 消费者下标
    int p_step;                 // 生产者下标
    Sem _space_sem;             // 空间资源
    Sem _data_sem;              // 数据资源
    pthread_mutex_t clock;      // 消费者的锁
    pthread_mutex_t plock;      // 生产者的锁
};

#endif
