#ifndef _Sem_HPP_
#define _Sem_HPP_

#include <iostream>
#include <semaphore.h>

class Sem
{
public:
    Sem(int Value)
    {
        sem_init(&_sem, 0, Value);
    }

    void p()
    {
        sem_wait(&_sem);
    }

    void v()
    {
        sem_post(&_sem);
    }

    ~Sem()
    {
        sem_destroy(&_sem);
    }

private:
    sem_t _sem;
};

#endif