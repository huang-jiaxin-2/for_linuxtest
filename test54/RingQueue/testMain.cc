#include "ringQueue.hpp"
#include <ctime>
#include <sys/types.h>
#include <unistd.h>

// 消费者
void *consumer(void *args)
{
    RingQueue<int> *rq = (RingQueue<int> *)args;
    while (true)
    {
        int x;
        // 1.从环形队列中获取任务或者数据
        rq->pop(&x);
        // 2.进行数据处理(有时间消耗问题)
        std::cout << "消费: " << x << " [" << pthread_self() << "]" << std::endl;
        sleep(1);
    }

    return nullptr;
}

// 生产者
void *producter(void *args)
{
    RingQueue<int> *rq = (RingQueue<int> *)args;
    while (true)
    {
        // 1.构建数据或者任务对象(有时间消耗问题)
        int x = rand() % 100 + 1;
        std::cout << "生产: " << x << " [" << pthread_self() << "]" << std::endl;

        // 2.推送到环形队列中
        rq->push(x);
    }

    return nullptr;
}

int main()
{
    srand((uint64_t)time(nullptr) ^ getpid());
    RingQueue<int> *rq = new RingQueue<int>();
    pthread_t c[4], p[4];

    // 让消费者和生产者看到同一份资源——环形队列
    for (int i = 0; i < 4; i++)
    {
        pthread_create(c + i, nullptr, consumer, (void *)rq);
        pthread_create(p + i, nullptr, producter, (void *)rq);
    }

    for (int i = 0; i < 4; i++)
    {
        pthread_join(c[i], nullptr);
        pthread_join(p[i], nullptr);
    }

    return 0;
}