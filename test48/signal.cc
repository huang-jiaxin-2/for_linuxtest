#include <iostream>
#include <vector>
#include <functional>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

using namespace std;

typedef function<void()> func;
vector<func> callbacks;

int count = 0;

// 自定义捕捉信号
void showCount()
{
    // cout << "进程捕捉到了一个信号，正在处理中: " << signum << " pid: " << getpid() << endl;
    cout << "count: " << count << endl;
}

void showlog()
{
    cout << "这是我的日志信息" << endl;
}

void showwho()
{
    if(fork() == 0)
    {
        execl("/usr/bin/who", "who", nullptr);
    }
    wait(nullptr);
}

void handle(int signum)
{
    sleep(1);
    cout << "捕获到一个信号: " << signum << endl;
    exit(1);
}

void catchsig(int signum)
{
    for(auto& f : callbacks)
    {
        f();
    }
    alarm(1);
}

int main()
{
    // signal函数,仅仅是修改了进程对特定信号的后续处理动作,不是直接调用对应的处理动作
    signal(SIGFPE, handle);//catchsig:回调函数

    int a = 100;
    a /= 0;

    while(true) sleep(1);

    // alarm(1);
    // callbacks.push_back(showCount);
    // callbacks.push_back(showlog);
    // callbacks.push_back(showwho);
    // while (true)
    // {
        // count++;
    // }

    // while(true)
    // {
    // cout << "我是一个进程，我正在运行……，pid: " << getpid() << endl;
    // sleep(1);

    // int a = 10;
    // a /= 0;
    // }

    // pid_t id = fork();
    // if(id == 0)
    // {
    // sleep(1);
    // int a = 100;
    // a /= 0;
    // exit(1);
    // }
    // int status = 0;
    // waitpid(id, &status, 0);
    // cout << "父进程pid: " << getpid() << " 子进程id: " << id << " 退出信号: " << (status & 0x7F) << " core dump: " << ((status >> 7) & 1) << endl;
    return 0;
}