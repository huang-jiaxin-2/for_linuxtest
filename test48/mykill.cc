#include <iostream>
#include <string>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>

using namespace std;

void Usage(string proc)
{
    cout << "Usage:\r\n\t" << proc << " signumber processid" << endl;
}

int main(int argc, char* argv[])
{
    cout << "我开始执行我的代码了" << endl;
    sleep(1);
    abort();

    // raise(8);//自己给自己发送信号
// 
    // if(argc != 3)
    // {
        // Usage(argv[0]);
        // exit(1);
    // }
// 
    // int signumber = atoi(argv[1]);
    // int processid = atoi(argv[2]);
// 
    // kill(processid, signumber);
    return 0;
}
