#include <string>

class ContactsException
{
public:
    ContactsException(std::string str = "A problem") : message(str){}
    std::string what() const
    {
        return message;
    }
private:
    std::string message;
};