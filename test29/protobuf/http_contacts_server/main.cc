#include <iostream>
#include "httplib.h"
#include "ContactsException.h"
#include "add_contact.pb.h"
#include "Utils.h"

using namespace std;
using namespace httplib;

void printContact(add_contact::AddContactRequest &req)
{
    cout << "联系人姓名:" << req.name() << endl;
    cout << "联系人年龄:" << req.age() << endl;
    for (int j = 0; j < req.phone_size(); j++)
    {
        const add_contact::AddContactRequest_Phone &phone = req.phone(j);
        cout << "联系人电话" << j + 1 << ":" << phone.number();
        cout << "    (" << phone.PhoneType_Name(phone.type()) << ")" << endl;
    }
}

int main()
{
    cout << "--------------服务启动---------------" << endl;
    Server server;

    server.Post("/contact/add", [](const Request &req, Response &res)
                {
                    cout << "接收到post请求!" << endl;
                    // 反序列化 request:req.bady
                    add_contact::AddContactRequest request;
                    add_contact::AddContactResponse response;
                    try
                    {
                        if (!request.ParseFromString(req.body))
                        {
                            throw ContactsException("AddcontactRequest反序列化失败!");
                        }

                        // 打印新增的联系人信息
                        printContact(request);

                        // 构造 response: res.bady
                        response.set_success(true);
                        response.set_uid(Utils::generate_hex(10));

                        // 序列化response
                        string response_str;
                        if (!response.SerializeToString(&response_str))
                        {
                            throw ContactsException("AddcontactResponse序列化失败!");
                        }
                        res.status = 200;
                        res.body = response_str;
                        res.set_header("Content-Type", "application/protobuf");
                    }
                    catch (const ContactsException &e)
                    {
                        res.status = 500;
                        response.set_success(false);
                        response.set_error_desc(e.what());
                        string response_str;
                        if (response.SerializePartialToString(&response_str))
                        {
                            res.body = response_str;
                            res.set_header("Content-Type", "application/protobuf");
                        }
                        cout << "/contacts/add 发生异常，异常信息：" << e.what() << endl;
                    } });

    server.listen("0.0.0.0", 8181);
    return 0;
}

// int main()
// {
    // cout << "--------------服务启动---------------" << endl;
    // Server server;
// 
    // server.Post("/test-post", [](const Request &req, Response &res)
                // {
// cout << "接收到post请求!" << endl;
// res.status = 200; });
// 
    // server.Get("/test-get", [](const Request &req, Response &res)
            //    {
// cout << "接收到get请求!" << endl;
// res.status = 200; });
// 
    // server.listen("0.0.0.0", 8181);
    // return 0;
// }