#include <sstream>
#include <random>
#include <google/protobuf/map.h>

class Utils
{
public:
    static unsigned int random_char()
    {
        // ⽤于随机数引擎获得随机种⼦
        std::random_device rd;
        // mt19937是c++11新特性，它是⼀种随机数算法，⽤法与rand()函数类似，但是mt19937
        // 具有速度快，周期⻓的特点
        // 作⽤是⽣成伪随机数
        std::mt19937 gen(rd());
        // 随机⽣成⼀个整数i 范围[0, 255]
        std::uniform_int_distribution<> dis(0, 255);
        return dis(gen);
    }
    // ⽣成 UUID （通⽤唯⼀标识符）
    static std::string generate_hex(const unsigned int len)
    {
        std::stringstream ss;
        // ⽣成 len 个16进制随机数，将其拼接⽽成
        for (auto i = 0; i < len; i++)
        {
            const auto rc = random_char();
            std::stringstream hexstream;
            hexstream << std::hex << rc;
            auto hex = hexstream.str();
            ss << (hex.length() < 2 ? '0' + hex : hex);
        }
        return ss.str();
    }

    static void map_copy(google::protobuf::Map<std::string, std::string> *target, const google::protobuf::Map<std::string, std::string> &source)
    {
        if (nullptr == target)
        {
            std::cout << "map_copy warning, target is nullptr!" << std::endl;
            return;
        }
        for (auto it = source.cbegin(); it != source.cend(); ++it)
        {
            target->insert({it->first, it->second});
        }
    }
};