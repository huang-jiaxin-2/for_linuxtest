#include <iostream>
#include <fstream>
#include "contacts.pb.h"

using namespace std;

void PrintContacts(contacts::Contacts &contacts)
{
    for (int i = 0; i < contacts.contacts_size(); i++)
    {
        cout << "------------联系人" << i + 1 << "------------" << endl;
        const contacts::PeopleInfo &people = contacts.contacts(i);
        cout << "联系人姓名:" << people.name() << endl;
        cout << "联系人年龄:" << people.age() << endl;
        for (int j = 0; j < people.phone_size(); j++)
        {
            const contacts::PeopleInfo_Phone &phone = people.phone(j);
            cout << "联系人电话" << j + 1 << ":" << phone.number();
            cout << "    (" << phone.PhoneType_Name(phone.type()) << ")" << endl;
        }
        if (people.has_data() && people.data().Is<contacts::Address>())
        {
            contacts::Address address;
            people.data().UnpackTo(&address);
            if (!address.home_address().empty())
            {
                cout << "联系人家庭地址:" << address.home_address() << endl;
            }
            if (!address.unit_address().empty())
            {
                cout << "联系人单位地址:" << address.unit_address() << endl;
            }
        }

        switch (people.other_contacts_case())
        {
        case contacts::PeopleInfo::OtherContactsCase::kQq:
            cout << "联系人QQ号:" << people.qq() << endl;
            break;
        case contacts::PeopleInfo::OtherContactsCase::kWechat:
            cout << "联系人微信号:" << people.wechat() << endl;
            break;
        default:
            break;
        }

        if(people.remark_size())
        {
            cout << "备注信息: " << endl;
        }
        for(auto it = people.remark().cbegin(); it != people.remark().cend(); it++)
        {
            cout << "   " << it->first << ": " << it->second << endl;
        }
    }
}

int main()
{
    contacts::Contacts contacts;
    // 读取本地已存在的通讯录文件
    fstream input("contacts.bin", ios::in | ios::binary);
    if (!contacts.ParseFromIstream(&input))
    {
        cerr << "parse error!" << endl;
        input.close();
        return -1;
    }

    // 打印通讯录列表
    PrintContacts(contacts);
    return 0;
}