#pragma once

#include <iostream>
#include <string>
#include <cstring>
#include <unordered_map>
#include <cerrno>
#include <cassert>
#include <memory>
#include <signal.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/wait.h>
#include <pthread.h>
#include <ctype.h>
#include "threadPool/log.hpp"
#include "threadPool/threadPool.hpp"
#include "threadPool/Task.hpp"

// static void service(int sock, const std::string &clientip, const uint16_t &clientport)
// {
    // char buffer[1024];
    // while (true)
    // {
        // ssize_t s = read(sock, buffer, sizeof(buffer) - 1);
        // if (s > 0)
        // {
            // buffer[s] = 0;
            // std::cout << clientip << ":" << clientport << "# " << buffer << std::endl;
        // }
        // else if (s == 0) // 对端关闭连接
        // {
            // logMessage(NORMAL, "%s:%d shutdown, me too", clientip.c_str(), clientport);
            // break;
        // }
        // else
        // {
            // logMessage(ERROR, "read socket error, %d:%s", errno, strerror(errno));
            // break;
        // }
// 
        // write(sock, buffer, strlen(buffer));
    // }
    // close(sock);
// }

static void service(int sock, const std::string &clientip, const uint16_t &clientport, const std::string &thread_name)
{
char buffer[1024];
while (true)
{
ssize_t s = read(sock, buffer, sizeof(buffer) - 1);
if (s > 0)
{
buffer[s] = 0;
std::cout << thread_name << "|" << clientip << ":" << clientport << "# " << buffer << std::endl;
}
else if (s == 0) // 对端关闭连接
{
logMessage(NORMAL, "%s:%d shutdown, me too", clientip.c_str(), clientport);
break;
}
else
{
logMessage(ERROR, "read socket error, %d:%s", errno, strerror(errno));
break;
}

write(sock, buffer, strlen(buffer));
}
close(sock);
}

// 小写转大写
static void change(int sock, const std::string &clientip, const uint16_t &clientport, const std::string &thread_name)
{
    char buffer[1024];
    ssize_t s = read(sock, buffer, sizeof(buffer) - 1);
    if (s > 0)
    {
        buffer[s] = 0;
        std::cout << thread_name << "|" << clientip << ":" << clientport << "# " << buffer << std::endl;
        std::string message;
        char *start = buffer;
        while (*start)
        {
            char c;
            if (islower(*start))
                c = toupper(*start);
            else
                c = *start;
            message.push_back(c);
            start++;
        }
        write(sock, message.c_str(), message.size());
    }
    else if (s == 0) // 对端关闭连接
    {
        logMessage(NORMAL, "%s:%d shutdown, me too", clientip.c_str(), clientport);
    }
    else
    {
        logMessage(ERROR, "read socket error, %d:%s", errno, strerror(errno));
    }

    close(sock);
}

//
static void dictOnline(int sock, const std::string &clientip, const uint16_t &clientport, const std::string &thread_name)
{
    char buffer[1024];
    static std::unordered_map<std::string, std::string> dict = {
        {"apple", "苹果"},
        {"banana", "香蕉"},
        {"hardly", "几乎不"},
        {"distance", "距离"},
        {"universal", "普遍的"}};
    ssize_t s = read(sock, buffer, sizeof(buffer) - 1);
    if (s > 0)
    {
        buffer[s] = 0;
        std::cout << thread_name << "|" << clientip << ":" << clientport << "# " << buffer << std::endl;
        std::string message;
        auto iter = dict.find(buffer);
        if (iter == dict.end())
            message = "查无此单词";
        else
            message = iter->second;
        write(sock, message.c_str(), message.size());
    }
    else if (s == 0) // 对端关闭连接
    {
        logMessage(NORMAL, "%s:%d shutdown, me too", clientip.c_str(), clientport);
    }
    else
    {
        logMessage(ERROR, "read socket error, %d:%s", errno, strerror(errno));
    }

    close(sock);
}

// class ThreadData
// {
// public:
    // int _sock;
    // std::string _ip;
    // uint16_t _port;
// };
// 
class TcpServer
{
private:
    const static int gbacklog = 20; // 不能太大不能太小
    // static void *threadRoutine(void *args)
    // {
        // pthread_detach(pthread_self()); // 进行分离
        // ThreadData *td = static_cast<ThreadData *>(args);
        // service(td->_sock, td->_ip, td->_port);
        // delete td;

        // return nullptr;
    // }

public:
    TcpServer(uint16_t port, std::string ip = "")
        : listensock(-1), _port(port), _ip(ip), _threadpool_ptr(ThreadPool<Task>::getThreadPool())
    {
    }
    void initServer()
    {
        // 1.创建socket
        listensock = socket(AF_INET, SOCK_STREAM, 0);
        if (listensock < 0)
        {
            logMessage(FATAL, "create socket error, %d:%s", errno, strerror(errno));
            exit(2);
        }
        logMessage(NORMAL, "create socket success, listensock: %d", listensock);
        // 2.bind
        struct sockaddr_in local;
        memset(&local, 0, sizeof local);
        local.sin_family = AF_INET;
        local.sin_port = htons(_port);
        local.sin_addr.s_addr = _ip.empty() ? INADDR_ANY : inet_addr(_ip.c_str());
        if (bind(listensock, (struct sockaddr *)&local, sizeof(local)) < 0)
        {
            logMessage(FATAL, "bind error, %d:%s", errno, strerror(errno));
            exit(3);
        }
        // 3.建立连接
        if (listen(listensock, gbacklog) < 0)
        {
            logMessage(FATAL, "listen error, %d:%s", errno, strerror(errno));
            exit(4);
        }

        logMessage(NORMAL, "init server success");
    }
    void Start()
    {
        // signal(SIGCHLD, SIG_IGN); // 忽略SIGCHLD信号，子进程退出时，会自动释放自己的僵尸状态
        _threadpool_ptr->run();
        while (true)
        {
            // sleep(1);
            // 4.获取连接
            struct sockaddr_in src;
            socklen_t len = sizeof(src);
            int servicesock = accept(listensock, (struct sockaddr *)&src, &len);
            if (servicesock < 0)
            {
                logMessage(ERROR, "accept error, %d:%s", errno, strerror(errno));
                continue;
            }
            uint16_t client_port = ntohs(src.sin_port);
            std::string client_ip = inet_ntoa(src.sin_addr);
            logMessage(NORMAL, "link success, servicesock: %d | %s : %d |\n", servicesock, client_ip.c_str(), client_port);
            // 通信
            //(1)单进程循环版--只能进行一次处理一个客户端
            // service(servicesock, client_ip, client_port);

            //(2.0)多进程--创建子进程
            // pid_t id = fork();
            // assert(id != -1);
            // if (id == 0)
            // {
            // close(listensock);
            // service(servicesock, client_ip, client_port);
            // exit(0);
            // }
            // close(servicesock);

            //(2.1)多进程--孤儿进程
            // pid_t id = fork();
            // if(id == 0)
            // {
            // close(listensock);
            // if(fork() > 0)
            // {
            // exit(0);
            // }
            // service(servicesock, client_ip, client_port);
            // exit(0);
            // }
            // waitpid(id, nullptr, 0);
            // close(servicesock);

            //(3)多线程
            // ThreadData *td = new ThreadData();
            // td->_sock = servicesock;
            // td->_ip = client_ip;
            // td->_port = client_port;
            // pthread_t tid;
            // pthread_create(&tid, nullptr, threadRoutine, td);
            

            //(4)线程池
            Task t(servicesock, client_ip, client_port, change);
            Task t(servicesock, client_ip, client_port, dictOnline);
            _threadpool_ptr->pushTask(t);
        }
    }
    ~TcpServer()
    {
    }

private:
    uint16_t _port;
    std::string _ip;
    int listensock;
    std::unique_ptr<ThreadPool<Task>> _threadpool_ptr;
};