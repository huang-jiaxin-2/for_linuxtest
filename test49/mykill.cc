#include <iostream>
#include <string>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>

using namespace std;

void Usage(string proc)
{
    cout << "Usage:\r\n\t" << proc << " processid" << endl;
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        Usage(argv[1]);
        exit(1);
    }
    int processid = atoi(argv[1]);

    for (int i = 1; i <= 31; i++)
    {
        if (i == 9 || i == 19)
            continue;
        kill(processid, i);
        cout << "kill -" << i << endl;
        sleep(1);
    }
    kill(processid, 9);
    return 0;
}