#include <iostream>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <cassert>
#include <sys/types.h>
#include <sys/wait.h>
using namespace std;

// int main()
// {
// signal(SIGCHLD, SIG_IGN);
// if(fork() == 0)
// {
// cout << "child: " << getpid() << endl;
// sleep(5);
// exit(1);
// }
//
// while(true)
// {
// cout << "parent: " << getpid() << " do my some thing!" << endl;
// sleep(1);
// }
// return 0;
// }

// void handler(int signum)
// {
// pid_t id;
// while((id = waitpid(-1, nullptr, WNOHANG)) > 0)
// {
// printf("wait child success:%d\n", id);
// }
// printf("child is quit! %d\n", getpid());
// }
//
// int main()
// {
// signal(SIGCHLD, handler);
// pid_t cid;
// if((cid = fork()) == 0)
// {
// printf("child: %d\n", getpid());
// sleep(3);
// exit(1);
// }
// while(1)
// {
// printf("father proc is doing some thing!\n");
// sleep(1);
// }
// return 0;
// }

// void handler(int signum)
// {
// cout << "子进程退出信号: " << signum << endl;
// }
//
// int main()
// {
// signal(SIGCHLD, handler);
// if(fork() == 0)
// {
// sleep(2);
// exit(1);
// }
//
// while(true) sleep(1);
// }

volatile int flag = 0;

void changeFlag(int signum)
{
    (void)signum;
    cout << "change flag: " << flag;
    flag = 1;
    cout << "->" << flag << endl;
}

int main()
{
    signal(2, changeFlag);
    while (!flag);
    cout << "进程正常退出后: " << flag << endl;
    return 0;
}

// static void printPending(sigset_t &pending)
// {
// for (int i = 1; i <= 31; i++)
// {
// if (sigismember(&pending, i))
// cout << "1";
// else
// cout << "0";
// }
// cout << endl;
// }
//
// void handler(int signum)
// {
// cout << "捕捉到一个信号: " << signum << endl;
//
// int count = 20;
// sigset_t pending;
// while (true)
// {
// 获取pending信号集
// sigpending(&pending);
// 打印信号集
// printPending(pending);
// count--;
// if (!count)
// break;
// sleep(1);
// }
// }

// int main()
// {
// cout << "pid: " << getpid() << endl;
// struct sigaction act, oact;
// act.sa_flags = 0;
// sigemptyset(&act.sa_mask);
//
// sigaddset(&act.sa_mask, 3);
// sigaddset(&act.sa_mask, 4);
// sigaddset(&act.sa_mask, 5);
// sigaddset(&act.sa_mask, 6);
//
// act.sa_handler = handler;
//
// sigaction(2, &act, &oact);
//
// cout << "default action: " << (int)(oact.sa_handler) << endl;
//
// while (true)
// sleep(1);
// }

// void handle(int signum)
// {
// cout << "捕捉到信号: " << signum << endl;
// }
//
// static void printPending(sigset_t &pending)
// {
// for (int i = 1; i <= 31; i++)
// {
// if (sigismember(&pending, i))
// cout << "1";
// else
// cout << "0";
// }
// cout << endl;
// }
//
// static void blockSig(int sig)
// {
// sigset_t bset;
// sigemptyset(&bset);
// sigaddset(&bset, sig);
// int n = sigprocmask(SIG_BLOCK, &bset, nullptr);
// assert(n == 0);
// (void)n;
// }
//
// int main()
// {
// cout << "pid: " << getpid() << endl;
// for (int sig = 1; sig <= 31; sig++)
// {
// blockSig(sig);
// }
// sigset_t pending;
// while (true)
// {
// 获取pending信号集
// sigpending(&pending);
// 打印信号集
// printPending(pending);
// sleep(1);
// }
// }
//
// int main()
// {
// 捕捉2号信号，不让其退出
// signal(2, handle);
// 1、定义信号集对象
// sigset_t bset, obset;
// sigset_t pending;
//
// 2、初始化
// sigemptyset(&bset);
// sigemptyset(&obset);
// sigemptyset(&pending);
//
// 3、添加要屏蔽的信号
// sigaddset(&bset, 2 /*SIGINT*/);
//
// 4、设置set到内核中对应的进程内部
// int n = sigprocmask(SIG_BLOCK, &bset, &obset);
// assert(n == 0);
// (void)n;
// cout << "block 2号信号成功……, pid: " << getpid() << endl;
//
// 5、重复打印当前进程的pending信号集
// int count = 0;
// while (true)
// {
// 获取pending信号集
// sigpending(&pending);
// 打印信号集
// printPending(pending);
// sleep(1);
// count++;
// count == 20时解除对2号信号的阻塞
// if (count == 20)
// {
// n = sigprocmask(SIG_SETMASK, &obset, nullptr);
// assert(n == 0);
// (void)n;
// cout << "解除2号信号的block成功" << endl;
// }
// }
//
// return 0;
// }

// void catchSig(int signum)
// {
// cout << "获取一个信号: " << signum << endl;
// }
//
// int main()
// {
// for (int i = 1; i <= 31; i++)
// signal(i, catchSig);
//
// while (true)
// sleep(1);
//
// sigaction();
// signal(2, SIG_DFL); // SIG_DFL:默认处理
// signal(2, SIG_IGN); // SIG_IGN:忽略
// return 0;
// }