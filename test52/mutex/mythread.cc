#include <iostream>
#include <pthread.h>
#include <unistd.h>
#include <assert.h>
#include <time.h>
using namespace std;

// pthread_mutex_t mtx = PTHREAD_MUTEX_INITIALIZER;//全局锁进行初始化
// int tickets = 1000; // 临界资源

// class ThreadData
// {
// public:
    // ThreadData(string &name, pthread_mutex_t *pmtx) : _tname(name), _pmtx(pmtx)
    // {
    // }
// 
// public:
    // string _tname;
    // pthread_mutex_t *_pmtx;
// };
// 
// 
// void *getTickets(void *args)
// {
    // ThreadData* td = (ThreadData*)args;
    // while (true)
    // {
        // int n = pthread_mutex_lock(td->_pmtx); // 加锁保护临界区资源
        // assert(n == 0);
        // if (tickets > 0)
        // {
            // usleep(1000);
            // printf("%s : %d\n", td->_tname.c_str(), tickets);
            // cout << td->_tname << " : " << tickets << endl;
            // tickets--;
            // n = pthread_mutex_unlock(td->_pmtx);
            // assert(n == 0);
        // }
        // else
        // {
            // n = pthread_mutex_unlock(td->_pmtx);
            // assert(n == 0);
            // break;
        // }
        // 处理后续的动作
        // cout << "恭喜，抢票成功" << endl;
        // usleep(1000);
    // }

    // return nullptr;
// }

// #define THREAD_NUM 5

// int main()
// {
    // pthread_mutex_t mtx;
    // pthread_mutex_init(&mtx, nullptr); // 局部定义的锁进行初始化的形式
    // pthread_t tid1, tid2, tid3, tid4;
    // pthread_create(&tid1, nullptr, getTickets, (void *)"thread 1");
    // pthread_create(&tid2, nullptr, getTickets, (void *)"thread 2");
    // pthread_create(&tid3, nullptr, getTickets, (void *)"thread 3");
    // pthread_create(&tid4, nullptr, getTickets, (void *)"thread 4");

    // pthread_t tid[THREAD_NUM];
    // for (int i = 0; i < THREAD_NUM; i++)
    // {
        // string name = "thread ";
        // name += to_string(i + 1);
        // ThreadData *td = new ThreadData(name, &mtx);
        // pthread_create(tid + i, nullptr, getTickets, (void *)td);
    // }

    // for (int i = 0; i < THREAD_NUM; i++)
    // {
        // pthread_join(tid[i], nullptr);
    // }

    // pthread_mutex_destroy(&mtx); // 最后将锁释放掉
    // return 0;
// }


int tickets = 1000;

 void* getTickets(void* args)
 {
     (void)args;
     while(true)
     {
         if(tickets > 0)
         {
             usleep(1000);
             printf("%p: %d\n", pthread_self(), tickets);
             tickets--;
         }
         else
         {
            break;
         }
     }
     return nullptr;
 }

 int main()
 {
     pthread_t t1, t2, t3;
     pthread_create(&t1, nullptr, getTickets, nullptr);
     pthread_create(&t1, nullptr, getTickets, nullptr);
     pthread_create(&t1, nullptr, getTickets, nullptr);

     pthread_join(t1, nullptr);
     pthread_join(t2, nullptr);
     pthread_join(t3, nullptr);

     return 0;
 }