#include <iostream>
#include <pthread.h>
#include <string>
#include <unistd.h>
using namespace std;

#define NUM 4
typedef void (*func_t)(const string name, pthread_mutex_t *pmtx, pthread_cond_t *pcond);
volatile bool quit = false;

class ThreadData
{
public:
    ThreadData(string &name, func_t func, pthread_mutex_t *pmtx, pthread_cond_t *pcond)
    : _name(name), _func(func), _pmtx(pmtx), _pcond(pcond)
    {
    }

public:
    string _name;
    func_t _func;
    pthread_mutex_t *_pmtx;
    pthread_cond_t *_pcond;
};

void func1(const string name, pthread_mutex_t *pmtx, pthread_cond_t *pcond)
{
    while(!quit)
    {
        pthread_mutex_lock(pmtx);
        pthread_cond_wait(pcond, pmtx);//线程等待
        cout << name << " running... -- 1" << endl;
        // sleep(1);
        pthread_mutex_unlock(pmtx);
    }
}

void func2(const string name, pthread_mutex_t *pmtx, pthread_cond_t *pcond)
{
    while(!quit)
    {
        pthread_mutex_lock(pmtx);
        pthread_cond_wait(pcond, pmtx);//线程等待
        cout << name << " running... -- 2" << endl;
        // sleep(1);
        pthread_mutex_unlock(pmtx);
    }
}

void func3(const string name, pthread_mutex_t *pmtx, pthread_cond_t *pcond)
{
    while(!quit)
    {
        pthread_mutex_lock(pmtx);
        pthread_cond_wait(pcond, pmtx);//线程等待
        cout << name << " running... -- 3" << endl;
        // sleep(1);
        pthread_mutex_unlock(pmtx);
    }
}

void func4(const string name, pthread_mutex_t *pmtx, pthread_cond_t *pcond)
{
    while(!quit)
    {
        pthread_mutex_lock(pmtx);
        pthread_cond_wait(pcond, pmtx);//线程等待
        cout << name << " running... -- 4" << endl;
        // sleep(1);
        pthread_mutex_unlock(pmtx);
    }
}


void* Entry(void* args)
{
    ThreadData* tmp = (ThreadData*)args;
    tmp->_func(tmp->_name, tmp->_pmtx, tmp->_pcond);
    delete tmp;
    return nullptr;
}

int main()
{
    pthread_mutex_t mtx;
    pthread_cond_t cond;
    pthread_mutex_init(&mtx, nullptr);
    pthread_cond_init(&cond, nullptr);

    pthread_t tid[NUM];
    func_t funcs[NUM] = {func1, func2, func3, func4};
    for (int i = 0; i < NUM; i++)
    {
        string name = "thread ";
        name += to_string(i + 1);
        ThreadData* td = new ThreadData(name, funcs[i], &mtx, &cond);
        pthread_create(tid + i, nullptr, Entry, (void*)td);
    }

    int cnt = 10;
    while(cnt)
    {
        cout << "resume thread run code..." << cnt-- << endl;
        pthread_cond_signal(&cond);
        // pthread_cond_broadcast(&cond);
        sleep(1);
    }

    cout << "ctrl done" << endl;
    quit = true;
    pthread_cond_broadcast(&cond);

    for(int i = 0; i < NUM; i++)
    {
        pthread_join(tid[i], nullptr);
        cout << "pthread: " << tid[i] << " quit" << endl; 
    }

    pthread_mutex_destroy(&mtx);
    pthread_cond_destroy(&cond);
    return 0;
}