#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char* argv[])
{
  if(argc != 2)
  {
    printf("can not execute\n");
    exit(1);
  }

  printf("获取环境变量:MY_VALUE:%s\n", getenv("MA_VALUE"));
  if(strcmp(argv[1], "-a") == 0)
  {
    printf("hello 我是a\n");
  }
  else if(strcmp(argv[1], "-b") == 0)
  {
    printf("hello 我是b\n");
  }
  else 
  {
    printf("defalut\n");
  }
  return 0;
}
