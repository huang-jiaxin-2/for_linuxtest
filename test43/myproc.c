#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>

#define NUM 16
const char* myfile = "/home/hjx/for_linuxtest/test43/test.py";

int main(int argc, char* argv[], char* env[])
{

  char* const _env[NUM] = {
    (char*)"MY_VALUE=332335454",
    NULL 
  };
  pid_t id = fork();
  if(id < 0)
  {
    perror("fork");
    exit(2);//子进程创建失败
  }
  else if(id == 0)
  {
    //子进程
    printf("子进程开始执行,pid:%d\n", getpid());
    //execl("python", "python", "test.py", NULL);
    execlp("python", "python", "test.py", NULL);
    char* const _argv[NUM] = {
      (char*)"ls",
      (char*)"-l",
      NULL 
    };
    //execv("/usr/bin/ls", _argv);
    //execle(myfile, "mycmd", "-a", NULL, _env);
    //execvpe("ls", _argv, env);
    exit(1);//替换失败则终止进程
  }
  else 
  {
    //父进程
    printf("父进程开始执行,pid:%d\n", getpid());
    int status;
    pid_t wid = waitpid(-1, &status, 0);//阻塞等待
    if(wid > 0)
    {
      printf("wait success, exit code:%d\n", WEXITSTATUS(status));
    }
  }
  return 0;
}

//int main()
//{
//  printf("当前进程的开始代码\n");
//  execl("/usr/bin/ls", "ls", "-a", "-l", NULL);
//  printf("当前进程的结束代码\n");
//}


//int main()
//{
//  pid_t pid = fork();
//  if(pid < 0)
//  {
//    perror("fork");
//    exit(2);//进程创建失败
//  }
//  else if(pid == 0)
//  {
//    //子进程
//    printf("子进程开始运行,pid:%d\n", getpid());
//    //execl("/usr/bin/ls", "ls", "-a", "-l", NULL);
//    char* const _argv[NUM] = {
//      (char*)"ls",
//      (char*)"-a",
//      (char*)"-l",
//      NULL 
//    };
//    //execv("/usr/bin/ls", _argv);
//    //execlp("ls", "ls", "-a", "-l", NULL);
//    execvp("ls", _argv);
//    exit(1);
//  }
//  else 
//  {
//    //父进程
//    printf("父进程开始运行,pid:%d\n", getpid());
//    int status = 0;
//    pid_t wid = waitpid(-1, &status, 0);//阻塞等待
//    if(wid > 0)
//    {
//      printf("wait success, exit code:%d\n", WEXITSTATUS(status));
//    }
//  }
//  return 0;
//}



//int main()
//{
//  printf("当前进程的开始代码\n");
//  execl("/usr/bin/ls", "ls", "--color=auto", "-l", NULL);
//  printf("当前进程的借宿代码\n");
//
//  //int a = 0;
//  //a++;
//  //execl("/usr/bin/pwd", "pwd", NULL);
//  //printf("%d\n", a++);
//  return 0;
//}
