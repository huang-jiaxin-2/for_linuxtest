#include "common.hpp"

int main()
{
    key_t k = ftok(PATH_NAME, PROJ_ID);
    if (k < 0)
    {
        Log("create key failed", Error) << " client key : " << k << endl;
        exit(1);
    }
    Log("create key success", Debug) << " client key : " << k << endl;

    // 閼惧嘲褰囬崗鍙橀煩閸愬懎鐡�1锟�7
    int shmid = shmget(k, SHM_SIZE, 0);
    if (shmid < 0)
    {
        Log("create shm failed", Error) << " client key : " << k << endl;
        exit(2);
    }
    Log("create shm success", Debug) << " client key : " << k << endl;
    // sleep(10);

    char *shmaddr = (char *)shmat(shmid, nullptr, 0);
    if (shmaddr == nullptr)
    {
        Log("attach shm failed", Error) << " client key : " << k << endl;
        exit(3);
    }
    Log("attach shm success", Debug) << " client key : " << k << endl;
    // sleep(10);

    // char a = 'a';
    // for(; a <= 'c'; a++)
    // {
    // snprintf(shmaddr, SHM_SIZE-1,\
        // "hello server, 閹存垶妲搁崗璺虹暊鏉╂稓鈻奸敍灞惧灉閻ㄥ埦id: %d, inc: %c\n", getpid(),a);
    // sleep(5);
    // }

    int fd = OpenFIFO(FIFO_NAME, WRITE);

    while (true)
    {
        ssize_t s = read(0, shmaddr, SHM_SIZE - 1);
        if (s > 0)
        {
            shmaddr[s - 1] = 0;
            Signal(fd);
            if (strcmp(shmaddr, "quit") == 0)
                break;
        }
    }

    // 閸樿鍙ч懕锟�1锟�7
    int n = shmdt(shmaddr);
    assert(n != -1);
    (void)n;
    Log("detach shm success", Debug) << " client key : " << k << endl;
    // sleep(10);

    CloseFifo(fd);
    return 0;
}