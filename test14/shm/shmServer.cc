#include "common.hpp"

Init init;

string TransToHex(key_t k)
{
    char buffer[32];
    snprintf(buffer, sizeof buffer, "0x%x", k);
    return buffer;
}

int main()
{
    // 1. 閸掓稑缂撻崗顒€鍙￠惃鍒眅y閸婏拷1閿燂拷7
    key_t k = ftok(PATH_NAME, PROJ_ID);
    assert(k != -1);
    Log("create key done", Debug) << " server key : " << TransToHex(k) << endl;

    // 2. 閸掓稑缂撻崗鍙橀煩閸愬懎鐡�
    int shmid = shmget(k, SHM_SIZE, IPC_CREAT | IPC_EXCL | 0666);
    if (shmid == -1)
    {
        perror("shmget");
        exit(1);
    }
    Log("create shm done", Debug) << " shmid : " << shmid << endl;

    // sleep(10);
    //  3.鐏忓棙瀵氱€规氨娈戦崗鍙橀煩閸愬懎鐡ㄩ幐鍌涘复閸掓媽鍤滃杈╂畱閸︽澘娼冪粚娲？
    char *shmaddr = (char *)shmat(shmid, nullptr, 0);
    Log("attach shm done", Debug) << " shmid : " << shmid << endl;

    // sleep(10);

    int fd = OpenFIFO(FIFO_NAME, READ);

    for (;;)
    {
        Wait(fd);
        printf("%s\n", shmaddr);
        if (strcmp(shmaddr, "quit") == 0)
            break;
        //sleep(1);
    }

    // 4. 鐏忓棙瀵氱€规氨娈戦崗鍙橀煩閸愬懎鐡ㄩ敍灞肩矤閼奉亜绻侀惃鍕勾閸ь澁鎷风粚娲？娑擃厼骞撻崗瀹犱粓
    int n = shmdt(shmaddr);
    assert(n != -1);
    (void)n;
    Log("datach shm done", Debug) << " shmid : " << shmid << endl;
    // sleep(10);

    // 5.閸掔娀娅庨崗鍙橀煩閸愬懎鐡�
    n = shmctl(shmid, IPC_RMID, nullptr);
    assert(n != -1);
    (void)n;
    Log("delete shm done", Debug) << " shmid : " << shmid << endl;
    CloseFifo(fd);
    return 0;
}