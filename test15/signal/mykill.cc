#include <iostream>
#include <string>
#include <vector>
#include <functional>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

using namespace std;
// typedef function<void()> func;
//
// vector<func> callback;
//
// unsigned int count = 0;
//
// void showCount()
// {
// cout << "进程捕捉到了一个信号，正在处理中：" << signum << " pid: " << getpid() << endl;
// cout << "final count:" << count << endl;
// }
//
// 日志信息
// void showLog()
// {
// cout << "日志信息" << endl;
// }
//
// 登录用户
// void logUser()
// {
// if (fork() == 0)
// {
// execl("/usr/bin/who", "who", nullptr);
// exit(1);
// }
// wait(nullptr);
// }
//
// void catchSig(int signum)
// {
// for (auto &f : callback)
// {
// f();
// }
// alarm(1);
// }
//
// mykill
// static void Usage(string proc)
// {
// cout << "Usage:\r\n\t" << proc << " signumber processid" << endl;
// }

void handler(int signum)
{
    sleep(1);
    cout << "获得了一个信号: " << signum << endl;
    exit(1);
}

int main(int argc, char *argv[])
{
    signal(SIGSEGV, handler);
    int *p = nullptr;
    *p = 100;
    // signal(SIGFPE, handler);
    // int a = 100;
    // a /= 0;

    // while (true)
    // sleep(1);

    // alarm(1); // 闹钟一旦触发,就自动移除了

    // signal(SIGALRM, catchSig);
    // callback.push_back(showCount);
    // callback.push_back(showLog);
    // callback.push_back(logUser);
    // while (true)
    // count++;

    // cout << "我开始运行了" << endl;

    // abort();//通常用来进行终止进程
    // sleep(1);
    // raise(8);

    // if(argc != 3)
    // {
    // Usage(argv[0]);
    // exit(1);
    // }

    // int signumber = atoi(argv[1]);
    // int procid = atoi(argv[2]);

    // kill(procid, signumber);

    // signal(SIGINT, catchSig);//2号信号

    // signal(SIGQUIT, catchSig);//3号信号
    // while(true)
    // {
    // cout << "我是一个进程，我正在运行..., pid: " << getpid() << endl;
    // sleep(1);

    // int a = 10;
    // a /= 0;
    // cout << "core here ..." << endl;
    // }

    // pid_t id = fork();
    // if(id == 0)
    // {
    // sleep(1);
    // int a = 100;
    // a /= 0;
    // exit(0);
    // }

    // int status = 0;
    // waitpid(id, &status, 0);
    // cout << "父进程: " << getpid() << " 子进程: " << id << \
    // " exit signal: " << (status & 0x7F) << " is core: " << ((status >> 7) & 1) << endl;
    // return 0;
}