#include "comm.hpp"

int main()
{
    //获取管道文件
    int fd = open(ipcPath.c_str(), O_WRONLY);
    if(fd < 0)
    {
        perror("open");
        exit(1);
    }

    //发送消息
    string buffer;
    while(true)
    {
        cout << "Please Enter Message Line > ";
        getline(cin, buffer);
        write(fd, buffer.c_str(), buffer.size());
    }

    //关闭文件描述符
    close(fd);
    return 0;
}