#include "comm.hpp"
#include <sys/wait.h>

static void getMessage(int fd)
{
    // 进行通信操作
    char buffer[SIZE];
    while (true)
    {
        memset(buffer, '\0', sizeof buffer);
        ssize_t s = read(fd, buffer, sizeof(buffer) - 1);
        if (s > 0)
        {
            // 读取成功
            cout << "[" << getpid() << "] "
                 << "client say> " << buffer << endl;
        }
        else if (s == 0)
        {
            // 读到文件结尾
            cerr << "[" << getpid() << "] "
                 << "read end of file, client quit, server quit too!" << endl;
            break;
        }
        else
        {
            // 读取失败
            perror("read");
            break;
        }
    }
}

int main()
{
    // 创建管道文件
    if (mkfifo(ipcPath.c_str(), MODE) < 0)
    {
        perror("mkfifo");
        exit(1);
    }
    logMessage(NORMAL, "创建管道文件成功, step 1"); // 打印日志

    // 打开管道文件
    int fd = open(ipcPath.c_str(), O_RDONLY);
    if (fd < 0)
    {
        perror("open");
        exit(2);
    }
    logMessage(NORMAL, "打开管道文件成功, step 2");

    int nums = 5;
    for (int i = 0; i < nums; i++)
    {
        pid_t id = fork();
        if (id == 0)
        {
            // 子进程抢占式读取消息
            getMessage(fd);
            exit(1);
        }
    }

    // 等待子进程退出
    for (int i = 0; i < nums; i++)
    {
        waitpid(-1, nullptr, 0);
    }

    // 关闭文件
    close(fd);
    logMessage(NORMAL, "关闭管道文件成功, step 3");

    // 通信完成，将管道文件删除
    unlink(ipcPath.c_str());
    logMessage(NORMAL, "删除管道文件成功, step 4");
    return 0;
}