#include "comm.hpp"

Init init;

int main()
{
    // 创建公共的key值
    key_t key = ftok(PATH_NAME, PROJ_ID);
    assert(key != -1);
    logMessage(NORMAL, "create key finish, server key: %d", key);

    // 创建一个全新的共享内存
    int shmid = shmget(key, SHM_SIZE, IPC_CREAT | IPC_EXCL | 0666); // 0666权限
    if (shmid == -1)
    {
        perror("shmget");
        exit(1);
    }
    logMessage(NORMAL, "create shmid finish, shmid: %d", shmid);

    // 将指定的共享内存挂接到自己的地址空间(建立映射)
    char *shmaddr = (char *)shmat(shmid, nullptr, 0);
    logMessage(NORMAL, "attach shm finish, shmid: %d", shmid);
    // sleep(10);

    int fd = OpenFIFO(FIFO_NAME, READ);
    // 读取数据
    for (;;)
    {
        Wait(fd);
        printf("%s\n", shmaddr);
        if(strcmp(shmaddr, "quit") == 0) break;
        sleep(1);
    }

    // 将指定的共享内存，从自己的地址空间中去关联
    int n = shmdt(shmaddr);
    assert(n != -1);
    (void)n;
    logMessage(NORMAL, "detach shm finish, shmid: %d", shmid);

    // 删除共享内存
    int m = shmctl(shmid, IPC_RMID, nullptr);
    assert(m != -1);
    (void)m;
    logMessage(NORMAL, "create shmid finish, shmid: %d", shmid);
    
    CloseFifo(fd);
    return 0;
}