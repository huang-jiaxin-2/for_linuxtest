#include "comm.hpp"

int main()
{
    logMessage(NORMAL, "client pid: %d", getpid());
    //获取key值
    key_t key = ftok(PATH_NAME, PROJ_ID);
    if (key < 0)
    {
        logMessage(ERROR, "create key fail");
        exit(1);
    }
    logMessage(NORMAL, "create key success, client key: %d", key);

    // 创建共享内存
    int shmid = shmget(key, SHM_SIZE, IPC_CREAT); // 0666权限
    if (shmid < 0)
    {
        logMessage(ERROR, "create shmget fail");
        exit(2);
    }
    logMessage(NORMAL, "create shmget success, shmid: %d", shmid);

    // 与地址空间建立关联
    char *shmaddr = (char *)shmat(shmid, nullptr, 0);
    if (shmaddr == nullptr)
    {
        logMessage(ERROR, "create shmat fail");
        exit(3);
    }
    logMessage(NORMAL, "attach shmat success, shmid: %d", shmid);

    int fd = OpenFIFO(FIFO_NAME, WRITE);
    // 写入数据
    while(true)
    {
        //加入管道部分
        ssize_t s = read(0, shmaddr, SHM_SIZE - 1);
        if(s > 0)
        {
            shmaddr[s - 1] = 0;
            Signal(fd);
            if(strcmp(shmaddr, "quit") == 0) break;
        }


        // char buffer[1024];
        // cout << "say >";
        // cin >> buffer;
        // if(strcmp(buffer, "quit") == 0) break;
        // snprintf(shmaddr, SHM_SIZE - 1,  "hello server, 我正在和你通信, pid: %d, client say > %s\n", getpid(), buffer);
    }

   //strcpy(shmaddr, "quit");
   CloseFifo(fd);

    // 去除与地址空间的关联
    int n = shmdt(shmaddr);
    assert(n != -1);
    (void)n;
    logMessage(NORMAL, "detach shmdt success, shmid: %d", shmid);

    return 0;
}