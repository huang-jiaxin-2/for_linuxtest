#pragma once

#include <iostream>
#include <cstdio>
#include <cassert>
#include <cstring>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "log.hpp"

using namespace std;

#define PATH_NAME "/home/hjx"
#define PROJ_ID 0x66
#define SHM_SIZE 4096 //共享内存的大小，最好是页的整数倍
#define FIFO_NAME "./myfifo"

//加入管道对共享内存进行访问控制
class Init
{
public:
    Init()
    {
        umask(0);
        int n = mkfifo(FIFO_NAME, 0666);
        assert(n == 0);
        (void)n;
        logMessage(NORMAL, "create fifo success");
    }
    ~Init()
    {
        unlink(FIFO_NAME);
        logMessage(NORMAL, "remove fifo success");
    }
};


#define READ O_RDONLY
#define WRITE O_WRONLY

int OpenFIFO(string pathname, int flags)
{
    int fd = open(pathname.c_str(), flags);
    assert(fd >= 0);
    return fd;
}

void Wait(int fd)
{
    logMessage(NORMAL, "等待中……");
    uint32_t temp = 0;
    ssize_t s = read(fd, &temp, sizeof(int32_t));
    assert(s == sizeof(uint32_t));
    (void)s;
}

void Signal(int fd)
{
    uint32_t temp = 1;
    ssize_t s = write(fd, &temp, sizeof(uint32_t));
    assert(s == sizeof(uint32_t));
    (void)s;
    logMessage(NORMAL, "唤醒中……");
}

void CloseFifo(int fd)
{
    close(fd);
}