#include <iostream>
#include <memory>
#include <cassert>
#include <fstream>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "HttpServer.hpp"
#include "Usage.hpp"
#include "Util.hpp"

#define ROOT "./wwwroot"
#define HOMEPAGE "index.html"

void HandlerHttpServerRequest(int sockfd)
{
    // 1.读取请求
    char buffer[10240];
    ssize_t s = recv(sockfd, buffer, sizeof(buffer) - 1, 0);
    if (s > 0)
    {
        buffer[s] = 0;
        std::cout << buffer << "---------------\n"
                  << std::endl;
    }

    std::vector<std::string> vline;
    Util::cutString(buffer, "\n", &vline);

    std::vector<std::string> vblock;
    Util::cutString(vline[0], " ", &vblock);

    std::string file = vblock[1];

    std::string target = ROOT;
    if (file == "/")
        file = "/index.html";
    target += file;

    std::cout << target << std::endl;
    std::string content;
    std::ifstream in(target);
    if (in.is_open())
    {
        std::string line;
        while (std::getline(in, line))
        {
            content += line;
        }
        in.close();
    }

    std::string HttpResponse;
    if (content.empty())
    {
        HttpResponse = "HTTP/1.1 302 Found\r\n";
        HttpResponse += "Location: http://81.70.184.223:8080/a/b/404.html\r\n";
    }
    else
    {
        HttpResponse = "HTTP/1.1 200 ok\r\n";
        HttpResponse += ("Content-Type: text/html\r\n");
        HttpResponse += ("Content-Length: " + std::to_string(content.size()) + "\r\n");
        HttpResponse += "Set-Cookie:这是一个cookie\r\n";
    }
    HttpResponse += "\r\n";
    HttpResponse += content;

    // std::cout << "####start################" << std::endl;
    // for (auto &iter : vblock)
    // {
    // std::cout << "---" << iter << "\n"
    //   << std::endl;
    // }
    // std::cout << "#####end###############" << std::endl;
    // HttpResponse += "<html><h3>hello world</h3></html>";

    send(sockfd, HttpResponse.c_str(), HttpResponse.size(), 0);
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        Usage(argv[0]);
        exit(0);
    }
    std::unique_ptr<HttpServer> httpserver(new HttpServer(atoi(argv[1]), HandlerHttpServerRequest));
    httpserver->Start();
    return 0;
}