#pragma once

#include <iostream>
#include <string>

void Usage(const std::string &process)
{
    std::cout << "\nUsage: " << process << " port\n"
              << std::endl;
}