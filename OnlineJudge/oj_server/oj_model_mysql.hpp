#pragma once
// mysql版本
#include <iostream>
#include <string>
#include <cassert>
#include <vector>
#include <unordered_map>
#include <cstdlib>
#include <fstream>
#include "../comm/log.hpp"
#include "../comm/util.hpp"
#include <mysql/mysql.h>

// 根据题目列表(list)文件，加载所有的题目信息带内存中
// model:主要用来和数据进行交互，对外提供访问数据的接口

namespace ns_model
{
    using namespace std;
    using namespace ns_log;
    using namespace ns_util;

    struct Question
    {
        string number; // 题目编号
        string title;  // 题目的标题
        string star;   // 题目的难度：简单 中等 困难
        string desc;   // 题目的描述
        string header; // 题目预设给用户在线编辑器的代码
        string tail;   // 题目的测试用例，需要和header拼接，形成完整代码
        int cpu_limit; // 题目的时间要求（s）
        int mem_limit; // 题目的空间要求（KB）
    };

    const std::string oj_questions = "oj_questions";
    const std::string host = "127.0.0.1";
    const std::string user = "oj_client";
    const std::string passwd = "12345678";
    const std::string db = "oj";
    int port = 8081;

    class Model
    {
    public:
        Model()
        {
        }
        bool QueryMysql(const std::string &sql, vector<Question> *out)
        {
            //初始化mysql
            MYSQL *mysql = mysql_init(nullptr);

            //连接数据库
            if(nullptr == mysql_real_connect(mysql, host.c_str(), user.c_str(), passwd.c_str(), db.c_str(), port, nullptr, 0))
            {
                LOG(FATAL) << "连接数据库失败!" << "\n";
                return false;
            }
            LOG(INFO) << "连接数据库成功!" << "\n";

            mysql_set_character_set(mysql, "utf8");

            //执行SQL语句
            if(0 != mysql_query(mysql, sql.c_str()))
            {
                LOG(WARNING) << sql << " execute error!" << "\n";
                return false;
            }

            //提取结果
            MYSQL_RES *res = mysql_store_result(mysql);

            int rows = mysql_num_rows(res);//获得行数
            int cols = mysql_num_fields(res);//获端列数

            Question q;
            for(int i = 0; i < rows; i++)
            {
                MYSQL_ROW row = mysql_fetch_row(res);
                q.number = row[0];
                q.title = row[1];
                q.star = row[2];
                q.desc = row[3];
                q.header = row[4];
                q.tail = row[5];
                q.cpu_limit = atoi(row[6]);
                q.mem_limit = atoi(row[7]);

                out->push_back(q);
            }


            //关闭连接
            mysql_close(mysql);

            return true;
        }
        // 获取全部题目
        bool GetAllQuestions(vector<Question> *out)
        {
            std::string sql = "select * from ";
            sql += oj_questions;
            return QueryMysql(sql, out);
        }
        // 获取单个题目
        bool GetOneQuestion(const std::string &number, Question *q)
        {
            bool res = false;
            std::string sql = "select * from ";
            sql += oj_questions;
            sql += " where number=";
            sql += number;
            vector<Question> result;
            if(QueryMysql(sql, &result))
            {
                if(result.size() == 1)
                {
                    *q = result[0];
                    res = true;
                }
            }
            return res;
        }
        ~Model() {}
    };
}