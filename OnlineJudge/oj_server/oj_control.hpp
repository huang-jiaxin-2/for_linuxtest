#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <mutex>
#include <fstream>
// #include <regex>
#include <cassert>
#include <algorithm>
#include <jsoncpp/json/json.h>
//#include "oj_model.hpp"
#include "oj_model_mysql.hpp"
#include "../comm/httplib.h"
#include "../comm/log.hpp"
#include "../comm/util.hpp"
#include "oj_view.hpp"

namespace ns_control
{
    using namespace std;
    using namespace ns_log;
    using namespace ns_model;
    using namespace ns_util;
    using namespace ns_view;
    using namespace httplib;

    // 提供服务的主机
    class Machine
    {
    public:
        string ip;     // 编译服务的IP
        int port;      // 编译服务的port
        uint64_t load; // 编译服务的负载
        mutex *mtx;    // mutex是禁止拷贝的，所以定义成指针
    public:
        Machine() : ip(""), port(0), load(0), mtx(nullptr)
        {
        }
        ~Machine() {}

    public:
        // 提升主机负载
        void IncLoad()
        {
            if (mtx)
                mtx->lock();
            load++;
            if (mtx)
                mtx->unlock();
        }
        // 减少主机负载
        void DecLoad()
        {
            if (mtx)
                mtx->lock();
            load--;
            if (mtx)
                mtx->unlock();
        }
        //重置负载
        void ResetLoad()
        {
            if(mtx) mtx->lock();
            load = 0;
            if(mtx) mtx->unlock();
        }
        // 获取主机负载(无太大意义)
        uint64_t GetLoad()
        {
            uint64_t _load = 0;
            if (mtx)
                mtx->lock();
            _load = load;
            if (mtx)
                mtx->unlock();

            return _load;
        }
    };

    const string service_machine = "./conf/service_machine.conf";

    // 负载均衡模块
    class LoadBlance
    {
    private:
        // 提供编译服务的所有主机
        // 每一台主机都有自己的下标，充当当前主机的id
        vector<Machine> machines;
        // 所有在线的主机id
        vector<int> online;
        // 所有离线的主机id
        vector<int> offline;
        // 保证loadblance的数据安全
        mutex mtx;

    public:
        LoadBlance()
        {
            assert(LoadConf(service_machine));
            LOG(INFO) << "加载 " << service_machine << " 成功"
                      << "\n";
        }
        ~LoadBlance() {}

    public:
        // 加载
        bool LoadConf(const string &machine_conf)
        {
            ifstream in(machine_conf);
            if (!in.is_open())
            {
                LOG(FATAL) << " 加载: " << machine_conf << " 失败"
                           << "\n";
                return false;
            }

            string line;
            while (getline(in, line))
            {
                vector<string> tokens;
                StringUtil::SpiltString(line, &tokens, ":");
                if (tokens.size() != 2)
                {
                    LOG(WARNING) << " 切分 " << line << " 失败"
                                 << "\n";
                    continue;
                }
                Machine m;
                m.ip = tokens[0];
                m.port = atoi(tokens[1].c_str());
                m.load = 0;
                m.mtx = new mutex();

                online.push_back(machines.size());
                machines.push_back(m);
            }
            in.close();
            return true;
        }
        // 智能选择:
        // 1. 使用选择好的主机(更新该主机的负载)
        // 2. 可能需要离线该主机
        // 负载均衡的算法:
        // 1. 随机数+hash
        // 2. 轮询+hash
        bool SmartChoice(int *id, Machine **m)
        {
            mtx.lock();
            int online_num = online.size();
            if (online_num == 0)
            {
                mtx.unlock();
                LOG(FATAL) << "所有后端编译主机已离线，请运维的同事加快查看"
                           << "\n";
                return false;
            }
            // 通过遍历的方式，找到所有负载最小的机器
            *id = online[0];
            *m = &machines[online[0]];
            uint64_t min_load = machines[online[0]].GetLoad();
            for (int i = 1; i < online_num; i++)
            {
                uint64_t curr_load = machines[online[i]].GetLoad();
                if (min_load > curr_load)
                {
                    min_load = curr_load;
                    *id = online[i];
                    *m = &machines[online[i]];
                }
            }
            mtx.unlock();
            return true;
        }
        void OfflineMachine(int which)
        {
            mtx.lock();
            for (auto iter = online.begin(); iter != online.end(); iter++)
            {
                if (*iter == which)
                {
                    machines[which].ResetLoad();//离线是需要重置负载
                    online.erase(iter);
                    offline.push_back(which);
                    break;
                }
            }
            mtx.unlock();
        }
        void OnlineMachine()
        {
            mtx.lock();
            online.insert(online.end(), offline.begin(), offline.end());
            offline.erase(offline.begin(), offline.end());
            mtx.unlock();

            LOG(INFO) << "所有的主机都上线了" << "\n";
        }

        // 用于调试
        void ShowMachines()
        {
            mtx.lock();
            cout << "当前在线主机列表: ";
            for (auto &id : online)
            {
                cout << id << " ";
            }
            cout << endl;
            cout << "当前离线主机列表: ";
            for (auto &id : offline)
            {
                cout << id << " ";
            }
            cout << endl;
            mtx.unlock();
        }
    };

    // 核心业务控制逻辑
    class Control
    {
    private:
        Model model_;            // 提供后台数据
        View view_;              // 提供HTML渲染功能
        LoadBlance load_blance_; // 核心负载均衡器
    public:
        Control() {}
        ~Control() {}

    public:
        //恢复主机
        void RecoveryMachine()
        {
            load_blance_.OnlineMachine();
        }
        // 根据题目数据构建网页
        bool AllQuestions(string *html)
        {
            bool ret = true;
            vector<struct Question> all;
            if (model_.GetAllQuestions(&all))
            {
                sort(all.begin(), all.end(), [](const struct Question &q1, const struct Question &q2){
                    return atoi(q1.number.c_str()) < atoi(q2.number.c_str());
                });
                // 获取题目信息成功，将所有的题目数据构建成网页
                view_.AllExpandHtml(all, html);
            }
            else
            {
                *html = "获取题目失败,形成题目列表失败";
                ret = false;
            }
            return ret;
        }

        bool Question(const string &number, string *html)
        {
            bool ret = true;
            struct Question q;
            if (model_.GetOneQuestion(number, &q))
            {
                // 获取指定题目信息成功，将题目数据构建成网页
                view_.OneExpandHtml(q, html);
            }
            else
            {
                *html = "指定题目: " + number + "不存在!";
                ret = false;
            }
            return ret;
        }

        // 判题
        void Judge(const string &number, const string in_json, string *out_json)
        {
            //debug
            // LOG(WARNING) << in_json << " \nnumber: " << number << "\n";

            // 根据题目编号，拿到对应的题目细节
            struct Question q;
            model_.GetOneQuestion(number, &q);
            // 1.in_json进行反序列化，得到题目的id、用户提交的代码、input
            Json::Reader reader;
            Json::Value in_value;
            reader.parse(in_json, in_value);
            string code = in_value["code"].asString();
            // 2.重新拼接用户代码+测试用例代码，形成新的代码
            Json::Value compile_value;
            compile_value["input"] = in_value["input"].asString();
            compile_value["code"] = code + "\n" + q.tail;
            compile_value["cpu_limit"] = q.cpu_limit;
            compile_value["mem_limit"] = q.mem_limit;
            Json::FastWriter writer;
            string compile_string = writer.write(compile_value);

            // 3.选择负载最低的主机(一直选择，直到主机可用，否则就是全部挂掉)
            while (true)
            {
                int id = 0;
                Machine *m = nullptr;
                if (!load_blance_.SmartChoice(&id, &m))
                {
                    break;
                }
                // 4.发起HTTP请求，得到结果
                Client cli(m->ip, m->port);
                m->IncLoad();
                LOG(INFO) << " 选择主机成功，主机id: " << id << " 详情: " << m->ip << ":" << m->port << " 当前主机的负载是: " << m->GetLoad() << "\n";
                if (auto res = cli.Post("/compile_and_run", compile_string, "application/json;charset=utf-8"))
                {
                    // 5.将结果赋值给out_json
                    if (res->status == 200)
                    {
                        *out_json = res->body;
                        m->DecLoad();
                        LOG(INFO) << "请求编译和运行服务成功..."
                                  << "\n";
                        break;
                    }
                    m->DecLoad();
                }
                else
                {
                    // 请求失败
                    LOG(ERROR) << " 当前请求的主机id: " << id << " 详情: " << m->ip << ":" << m->port << " 可能已经离线"
                               << "\n";
                    load_blance_.OfflineMachine(id);

                    // 用于调试
                    load_blance_.ShowMachines();
                }
            }
        }
    };

}