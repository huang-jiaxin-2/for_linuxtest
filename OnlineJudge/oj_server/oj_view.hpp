#pragma once

#include <iostream>
#include <string>
#include <ctemplate/template.h>
//#include "oj_model.hpp"
#include "oj_model_mysql.hpp"

namespace ns_view
{
    using namespace ns_model;

    const string template_path = "./template_html/";
    class View
    {
    public:
        View() {}
        ~View() {}

    public:
        void AllExpandHtml(const vector<struct Question> &questions, string *html)
        {
            // 题目的编号 题目的标题 题目的难度
            // 使用表格显示
            // 1.形成路径
            string src_html = template_path + "all_questions.html";
            // 2.形成数据字典
            ctemplate::TemplateDictionary root("all_questions");
            for (const auto &q : questions)
            {
                ctemplate::TemplateDictionary *sub = root.AddSectionDictionary("questions_list");
                sub->SetValue("number", q.number);
                sub->SetValue("title", q.title);
                sub->SetValue("star", q.star);
            }
            // 3.获取被渲染的网页
            ctemplate::Template *tpl = ctemplate::Template::GetTemplate(src_html, ctemplate::DO_NOT_STRIP);
            // 4.添加字典数据到网页中
            tpl->Expand(html, &root);
        }
        void OneExpandHtml(const struct Question &q, string *html)
        {
            // 1.形成路径
            string src_html = template_path + "one_question.html";
            // 2.形成数据字典
            ctemplate::TemplateDictionary root("one_question");
            root.SetValue("number", q.number);
            root.SetValue("title", q.title);
            root.SetValue("star", q.star);
            root.SetValue("cpu_limit", to_string(q.cpu_limit));
            root.SetValue("mem_limit", to_string(q.mem_limit));
            root.SetValue("desc", q.desc);
            root.SetValue("pre_code", q.header);
            // 3.获取被渲染的网页
            ctemplate::Template *tpl = ctemplate::Template::GetTemplate(src_html, ctemplate::DO_NOT_STRIP);
            // 4.添加字典数据到网页中
            tpl->Expand(html, &root);
        }
    };
}