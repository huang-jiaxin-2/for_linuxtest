项目核心是三个模块：

1. comm: 公共模块
2. compile_server: 编译和运行模块
3. oj_server: 获取题目列表，查看题目编写题目界面，负载均衡，其它功能

///////////////////////////////////////////////////////////////////////////////////////

测试compile_run
#include "compile_run.hpp"

using namespace ns_compile_and_run;

int main()
{
    //充当客户端请求json串
    std::string in_json;
    Json::Value in_value;
    in_value["code"] = R"(#include<iostream>
    int main()
    { 
        std::cout << "你可以开始写代码了" << std::endl;
        bbbb
        return 0;
    })";
    in_value["input"] = "";
    in_value["cpu_limit"] = 1;
    in_value["mem_limit"] = 10240 * 3;
    Json::FastWriter writer;
    in_json = writer.write(in_value);

    // std::cout << in_json << std::endl;
    //给客户端返回json串
    std::string out_json;
    CompileAndRun::Start(in_json, &out_json);

    std::cout << out_json << std::endl;
    return 0;
}

/////////////////////////////////////////////////////////////////////////////////////

//测试httplib
Server svr;
// svr.Get("/hello", [](const Request &req, Response &resp)
// {
////用来进行基本测试
// resp.set_content("hello httplib,你好 httplib!", "text/plain;charset=utf-8"); });
svr.listen("0.0.0.0", 8080);

/////////////////////////////////////////////////////////////////////////////////////////////

//测试boost库
#include <iostream>
#include <boost/algorithm/string.hpp>

int main()
{
    std::vector<std::string> tokens;
    const std::string str = "1:判断回文数::::::简单:1:30000";
    const std::string sep = ":";
    boost::split(tokens, str, boost::is_any_of(sep), boost::algorithm::token_compress_on);

    for(auto &iter : tokens){
        std::cout << iter << std::endl;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////

//测试ctemplate 代码和html
// #include <iostream>
// #include <string>
// #include <ctemplate/template.h>

// int main()
// {
//     std::string in_html = "./test.html";
//     std::string value = "晚风依旧";

//     //形成数据字典
//     ctemplate::TemplateDictionary root("test"); //unordered_map<string,string> test;
//     root.SetValue("key", value);                //test.insert({key, value});

//     //获取被渲染网页对象
//     ctemplate::Template *tpl = ctemplate::Template::GetTemplate(in_html, ctemplate::DO_NOT_STRIP); //DO_NOT_STRIP：保持html网页原貌

//     //添加字典数据到网页中
//     std::string out_html;
//     tpl->Expand(&out_html, &root);

//     std::cout << out_html << std::endl;
// }

ctemplate渲染需要刷新

//html
<!-- <!DOCTYPE html> -->
<!-- <html lang="en"> -->
<!-- <head> -->
    <!-- <meta charset="UTF-8"> -->
    <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge"> -->
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0"> -->
    <!-- <title>用来测试</title> -->
<!-- </head> -->
<!-- <body> -->
    <!-- <p>{{key}}</p> -->
    <!-- <p>{{key}}</p> -->
    <!-- <p>{{key}}</p> -->
    <!-- <p>{{key}}</p> -->
    <!-- <p>{{key}}</p> -->
<!--  -->
<!-- </body> -->
<!-- </html> -->
/////////////////////////////////////////////////////////////////////////////////

