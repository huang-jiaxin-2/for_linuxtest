#include <iostream>
#include <assert.h>
#include <string>
#include <cstdio>
#include <cstring>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

using namespace std;

int main()
{
    // 1.创建管道
    int pipefd[2] = {0}; // pipefd[0]:读端 ， pipefd[1]: 写端
    int n = pipe(pipefd);
    assert(n != -1);
    (void)n;

#ifdef DEBUG
    cout << "pipefd[0]: " << pipefd[0] << endl;
    cout << "pipefd[1]: " << pipefd[1] << endl;
#endif
    // 2.创建子进程
    pid_t id = fork();
    assert(id != -1);
    if (id == 0)
    {
        // 子进程
        // 3.构建单向通信的信道，父进程写入，子进程读取
        // 关闭子进程不需要的fd
        close(pipefd[0]);
        // close(pipefd[0]);
        char buffer[1024 * 8];
        while (true)
        {
            // sleep(10);
            // 写入的一方，fd没有关闭，如果有数据，就读，没有数据就等
            // 写入的一方，fd关闭，读取的一方，read会返回0，表示读到了文件的结尾
            //  ssize_t s = read(pipefd[0], buffer, sizeof(buffer)-1);
            //  if(s > 0)
            //  {
            //  buffer[s] = 0;
            //  cout << " child get a messgae[" << getpid() << "] father#" << buffer << endl;
            // }
            // else if(s == 0)
            // {
            // cout << "write quit(father),me quit!!!" << endl;
            // break;
            // }

            string message = "我是子进程，我正在给你发消息";
            int count = 0;
            char send_buffer[1024];
            snprintf(send_buffer, sizeof(send_buffer), "%s[%d] : %d", message.c_str(), getpid(), count++);
            write(pipefd[1], send_buffer, strlen(send_buffer));
        }

        exit(0);
    }

    // 父进程
    // 3.构建单向通信的信道
    // 关闭父进程不需要的fd
    close(pipefd[0]);
    //close(pipefd[1]);
    // string message = "我是父进程，我正在给你发消息";
    // int count = 0;
    // char send_buffer[1024];
    // while (true)
    // {
        // 构建一个变化的字符串
        //snprintf(send_buffer, sizeof(send_buffer), "%s[%d] : %d", message.c_str(), getpid(), count++);
        // 写入
        //write(pipefd[1], send_buffer, strlen(send_buffer));

        // sleep(1);
        // cout << count << endl;
        //    if(count == 5)
        //    {
        //         cout << "write quit(father)" << endl;
        //         break;
        //    }
    //}

    int status = 0;
    pid_t ret = waitpid(id, &status, 0);
    assert(ret > 0);
    (void)ret;
    cout << "signumber: " << (status & 0x7F) << endl;
    close(pipefd[1]);
    return 0;
}
