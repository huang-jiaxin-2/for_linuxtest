#include "TcpServer.hpp"
#include "Protocol.hpp"
#include "Daemon.hpp"
#include <memory>
#include <signal.h>

using namespace ns_tcpserver;
using namespace ns_protocol;

static void Usage(const std::string &process)
{
    std::cout << "\nUsage: " << process << " port\n"
              << std::endl;
}

// void debug(int sock)
// {
// std::cout << "我是测试服务，我的sock: " << sock << std::endl;
// }

Response calculatorHelper(const Request &req)
{
    Response resp(0, 0, req.x_, req.y_, req.op_);
    switch (req.op_)
    {
    case '+':
        resp.result_ = req.x_ + req.y_;
        break;
    case '-':
        resp.result_ = req.x_ - req.y_;
        break;
    case '*':
        resp.result_ = req.x_ * req.y_;
        break;
    case '/':
        if (0 == req.y_)
            resp.code_ = 1;
        else
            resp.result_ = req.x_ / req.y_;
        break;
    case '%':
        if (0 == req.y_)
            resp.code_ = 2;
        else
            resp.result_ = req.x_ % req.y_;
        break;
    default:
        resp.code_ = 3;
        break;
    }
    return resp;
}

void calculator(int sock)
{
    std::string inbuffer;
    while (true)
    {
        // 1.读取成功
        bool res = Recv(sock, &inbuffer);
        if (!res)
            break;
        // 2.解析协议
        std::string package = Decode(inbuffer);
        if (package.empty())
            continue;

        logMessage(NORMAL, "%s", package.c_str());
        Request req;
        // 反序列化
        req.Deserialize(package);
        Response resp = calculatorHelper(req);
        // 序列化
        std::string respString = resp.Serialize();
        // 添加信息，形成报文
        respString = Encode(respString);
        Send(sock, respString);
    }
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        Usage(argv[0]);
        exit(1);
    }

    // 一般服务器，都是要忽略SIGPIPE信号的，防止在运行中出现非法写入问题
    // signal(SIGPIPE, SIG_IGN);

    MyDaemon();

    std::unique_ptr<TcpServer> server(new TcpServer(atoi(argv[1])));
    server->BindServer(calculator);
    server->start();

    // Request req(124, 234, '+');
    // std::string s = req.Serialize();
    // std::cout << s << std::endl;

    // Request temp;
    // temp.Deserialize(s);
    // std::cout << temp.x_ << std::endl;
    // std::cout << temp.op_ << std::endl;
    // std::cout << temp.y_ << std::endl;

    return 0;
}
