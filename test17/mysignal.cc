#include <iostream>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>

using namespace std;

void handler(int signum)
{
    pid_t id;
    while((id = waitpid(-1, nullptr, WNOHANG)) > 0)
    {
        cout << "wait child success: " << id << endl;
    }
    cout << "child id qiut " << getpid() << endl;
}

int main()
{
    signal(SIGCHLD, handler);
    if(fork() == 0)
    {
        //child
        cout << "child pid: " << getpid() << endl;
        sleep(3);
        exit(1);
    }

    while(1)
    {
        cout << "father proc is doing some thing" << endl;
        sleep(1);
    }
    return 0;
}


// 不等待子进程，并且让子进程退出后，自动释放僵尸进程
// int main()
// {
// signal(SIGCHLD, SIG_IGN);//手动设置对子进程进行忽略
// if (fork() == 0)
// {
// cout << "child: " << getpid() << endl;
// sleep(5);
// exit(0);
// }
//
// while (true)
// {
// cout << "father pid: " << getpid() << " 我在执行自己的进程" << endl;
// sleep(1);
// }
// return 0;
//}

// void handler(int signum)
// {
    // cout << "子进程退出: " << signum << endl;
    // cout << "father pid: " << getpid() << endl;
// }
// 
////子进程退出，会向父进程发送信号
// int main()
// {
    // signal(SIGCHLD, handler);
    // if (fork() == 0)
    // {
        // cout << "child pid: " << getpid() << endl;
        // sleep(100);
        // exit(0);
    // }
// 
    // while (true)
        // sleep(1);
    // return 0;
//  }
// 
    // int flag = 0;
    //  volatile int flag = 0;
    //
    //  void changeFlag(int signum)
    //  {
    //  (void)signum;
    //  cout << "change flag: " << flag;
    //  flag = 1;
    //  cout << "->" << flag << endl;
    // }
    //
    // int main()
    // {
    // signal(2, changeFlag);
    // while(!flag);
    // cout << "进程正常退出后: " << flag << endl;
    // return 0;
    // }

    //  void showPending(sigset_t *pending)
    // {
    // for (int sig = 1; sig <= 31; sig++)
    // {
    // if (sigismember(pending, sig))
    // cout << "1";
    // else
    // cout << "0";
    // }
    // cout << endl;
    // }
    //
    // void handler(int signum)
    // {
    // cout << "获取了一个信号: " << signum << endl;
    // cout << "获取了一个信号: " << signum << endl;
    //
    // sigset_t pending;
    // int c = 20;
    // while (true)
    // {
    // sigpending(&pending);
    // showPending(&pending);
    // c--;
    // if (!c)
    // break;
    // sleep(1);
    // }
    // }
    //
    // int main()
    // {
    // 内核数据类型，用户栈定义的
    // struct sigaction act, oldact;
    // act.sa_flags = 0;
    // sigemptyset(&act.sa_mask);
    // act.sa_handler = handler;

    // sigaddset(&act.sa_mask, 3);
    // sigaddset(&act.sa_mask, 4);
    // sigaddset(&act.sa_mask, 5);
    // sigaddset(&act.sa_mask, 6);

    // 设置进当前调用进程的pcb中
    // sigaction(2, &act, &oldact);

    // cout << "default action : " << (int)(oldact.sa_handler) << endl;
    // cout << "pid: " << getpid() << endl;

    // while (true)
    // sleep(1);
    // return 0;
    //}
