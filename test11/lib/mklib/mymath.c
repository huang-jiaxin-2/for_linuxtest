#include "mymath.h"

int addToTarget(int from, int to)
{
  int sum = 0;
  int i;
  for(i = from; i < to; i++)
  {
    sum+=i;
  }

  return sum;
}
