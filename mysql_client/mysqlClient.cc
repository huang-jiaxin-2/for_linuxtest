#include <iostream>
#include <cstdlib>
#include <string>
#include <cstdio>
#include <cstring>
#include <mysql/mysql.h>
using namespace std;

string host = "127.0.0.1";
string user = "hjx";
string passwd = "12345678";
string db = "hjx";
unsigned int port = 3306;

int main()
{
    // cout << "mysql client version:" << mysql_get_client_info() << endl;
    // 初始化对象
    MYSQL *mysql = mysql_init(nullptr);
    if (mysql == nullptr)
    {
        cerr << "mysql_init error" << endl;
        exit(1);
    }
    // 登录认证
    if (mysql_real_connect(mysql, host.c_str(), user.c_str(), passwd.c_str(), db.c_str(), port, nullptr, 0) == nullptr)
    {
        cerr << "mysql_real_connect error" << endl;
        exit(2);
    }
    mysql_set_character_set(mysql, "utf8");

    cout << "mysql_real_connect success" << endl;

    // string sql = "insert into emp values (100, '曹操', 7788.90);";
    // string delspl = "delete from emp where id = 100;";
    // string selectsql = "select * from emp;";

    char sql[1024];

    while (true)
    {
        printf("mysql> ");
        fgets(sql, sizeof sql, stdin);
        // 调用成功时，返回值为0，否则为1
        int n = mysql_query(mysql, sql);
        if (strcasestr(sql, "select") && n == 0)
        {
            cout << "result: " << n << endl;

            // 对结果进行解析
            MYSQL_RES *res = mysql_store_result(mysql);
            if (res == nullptr)
                exit(3);
            int rows = mysql_num_rows(res);
            int fields = mysql_num_fields(res);
            MYSQL_FIELD *fname = mysql_fetch_fields(res);
            for (int j = 0; j < fields; j++)
                cout << fname[j].name << "\t|\t";
            cout << endl;
            MYSQL_ROW line;
            for (int i = 0; i < rows; i++)
            {
                line = mysql_fetch_row(res);
                for (int j = 0; j < fields; j++)
                    cout << line[j] << "\t|\t";
                cout << endl;
            }

            printf("%d rows in set\n", rows);
        }
        else if(strcasestr(sql, "quit"))
        {
            cout << "Bye" << endl;
            break;
        }
        else 
        {
            cout << "execl sql : " << sql << " done" << endl;
        }
    }

    // 关闭对象
    mysql_close(mysql);
    return 0;
}