#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

#define NUM 16
//const char* file = "/home/hdd/for_linuxtest/test6/cmd";
const char* file = "./cmd";

int main()
{
  char *const _env[NUM] = {
    (char*)"MYCMD=777776666",
    NULL
  };

  pid_t id = fork();
  if(id == 0)
  {
    //子进程
    printf("子进程开始运行,pid: %d\n",getpid());
    sleep(3);
    char *const _argv[NUM] = {
      (char*)"ls",
      (char*)"-a",
      (char*)"-l",
      NULL
    };

    execle(file, "cmd", "-a", NULL, _env);
    //用c/c++调用其它函数的代码
     //execlp("bash","bash","test.sh",NULL);
    //execlp("python","python","test.py",NULL);
    //execv("/usr/bin/ls",_argv);
    //execl(file,"cmd","-a",NULL);
    //execlp("ls", "ls", "-a", "-l", NULL);
    //execvp("ls",_argv);
    exit(1);
  }
  else
  {
    //父进程
    printf("父进程开始运行,pid: %d\n",getpid());
    int status = 0;
    pid_t id = waitpid(-1, &status, 0);//阻塞等待
    if(id > 0)
    {
      printf("wait success, exit code: %d\n",WEXITSTATUS(status));
    }
  }
  return 0;
}
