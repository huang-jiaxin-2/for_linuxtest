#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>

#define NUM 1024
#define SIZE 32
#define SEP " "

#define INPUT_REDIR 1
#define OUTPUT_REDIR 2
#define APPEND_REDIR 3
#define NONE_REDIR 0

//保存完整的命令行字符串
char cmd_line[NUM];
//保存打散之后的命令行字符串
char* g_argv[SIZE];

char g_myval[64];

int redir_status = NONE_REDIR;

char *CheckRedir(char *start)
{
  assert(start);
  char *end = start + strlen(start) - 1;
  while(end > start)
  {
    if(*end == '>')
    {
      if(*(end-1) == '>')
      {
        redir_status = APPEND_REDIR;
        *(end-1) = '\0';
        end++;
        break;
      }
      redir_status = OUTPUT_REDIR;
      *end = '\0';
      end++;
      break;
    }
    else if(*end == '<')
    {
      redir_status = INPUT_REDIR;
      *end = '\0';
      end++;
      break;
    }
    else 
    {
      end--;
    }
  }
  if(end >= start)
  {
    return end;//要打开的文件
  }
  else {
    return NULL;
  }
}

//shell运行原理：通过让子进程这行命令，父进程等待并且解析命令
int main()
{
  extern char**environ;
  while(1)
  {
    //1.打印出提示信息
    printf("[root@我的主机 myshell]# ");
    fflush(stdout);
    memset(cmd_line, '\0', sizeof cmd_line);

    //2.获取用户的键盘输入(输入的是各种指令和选项)
    if(fgets(cmd_line, sizeof cmd_line, stdin) == NULL)
    {
      continue;
    }
    cmd_line[strlen(cmd_line)-1] = '\0';
    //printf("echo: %s\n", cmd_line);

    char *sep = CheckRedir(cmd_line);

    //3.命令行字符串解析
    g_argv[0] = strtok(cmd_line, SEP);//第一次调用，要传入原始字符串
    int index = 1;
    if(strcmp(g_argv[0], "ls") == 0)
    {
      g_argv[index++] = "--color=auto";
    }
    if(strcmp(g_argv[0], "ll") == 0)
    {
      g_argv[0] = "ls";
      g_argv[index++] = "-l";
      g_argv[index++] = "--color=auto";
    }
    while(g_argv[index++] = strtok(NULL, SEP));//第二次，如果还要解析字符串，传入NULL
    if(strcmp(g_argv[0], "export") == 0 && g_argv[1] != NULL)
    {
      strcpy(g_myval, g_argv[1]);
      int ret = putenv(g_myval);
      if(ret == 0)
      {
        printf("%s export success\n", g_argv[1]);
      }
      continue;
    }

    //debug
  //  for(index = 0; g_argv[index]; index++)
  //  {
  //    printf("g_argv[%d]: %s\n", index, g_argv[index]);
  //  }
  
    //4.TODO,内置命令(让父进程自己执行的命令)
    //本质就是shell中的一个函数调用
    if(strcmp(g_argv[0], "cd") == 0)
    {
      if(g_argv[1] != NULL)
      {
        chdir(g_argv[1]);
      }
      continue;
    }
    
    //5.fork()
    pid_t id = fork();
    if(id == 0)//子进程
    {
      if(sep != NULL)
      {
        int fd = -1;
        switch(redir_status)
        {
          case INPUT_REDIR:
            fd = open(sep, O_RDONLY);
            dup2(fd, 0);
            break;
          case OUTPUT_REDIR:
            fd = open(sep, O_WRONLY | O_TRUNC | O_CREAT, 0666);
            dup2(fd, 1);
            break;
          case APPEND_REDIR:
            fd = open(sep, O_WRONLY | O_APPEND | O_CREAT, 0666);
            dup2(fd, 1);
            break;
          default:
            //printf("bug?\n");
            break;
        }
      }

      //printf("下面功能让子进程执行的\n");

      execvp(g_argv[0], g_argv);
      exit(1);
    }
    //父进程
    int status = 0;
    pid_t ret = waitpid(id, &status, 0);
    if(ret > 0)
    {
      printf("exit code: %d\n", WEXITSTATUS(status));
    }
   }
  

  return 0;
}
