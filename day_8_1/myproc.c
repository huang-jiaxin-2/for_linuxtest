#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

int main()
{

  printf("I am parent process!!\n");
  
  // fork();

  pid_t ret = fork();
 //变成两个进程：一个父进程，一个子进程 
  printf("ret:%d,pid:%d,ppid:%d\n",ret,getpid(),getppid());
  //while(1)
  //{
  //  pid_t id = getpid();   //获取的是自己的进程PID
  //  printf("hello world，pid:%d,ppid:%d\n",id,getppid());
  //  sleep(1);
  // }
  return 0;
}
