#include <stdio.h>
#include <unistd.h>

int main()
{
  pid_t id = fork();
  if(id == 0)
  {
    while(1)
    {
      printf("hello child\n");
      sleep(1);
    }
  }
  else 
  {
    int cout = 5;
    while(cout)
    {
      printf("hello father:%d\n",cout);
      cout--;
      sleep(1);
    }
  }
  return 0;
}
