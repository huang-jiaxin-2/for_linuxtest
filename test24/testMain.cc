#include "threadPool.hpp"
#include "Task.hpp"
#include <ctime>
#include <cstdlib>
#include <iostream>
#include <unistd.h>

int main()
{
    //logMessage(NORMAL, "%s %d \n", "这是一条日志信息", 32434);
    srand((unsigned long)time(nullptr) ^ getpid());
    ThreadPool<Task> *tp = new ThreadPool<Task>();
    tp->run();
    while (true)
    {
        int x = rand() % 100 + 1;
        usleep(7654);
        int y = rand() % 300 + 1;
        Task t(x, y, [](int x, int y) -> int
               { return x + y; });

        //std::cout << "制作任务完成: " << x << "+" << y << "=?" << std::endl;
        logMessage(DEBUG, "制作任务完成: %d+%d=?", x, y);
        tp->pushTask(t);
        sleep(1);
    }
    return 0;
}