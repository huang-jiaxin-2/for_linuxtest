#include <iostream>
#include <string>
#include <unistd.h>
#include <pthread.h>

using namespace std;

#define TNUM 4
typedef void (*func_t)(const string &name, pthread_mutex_t *pmtx, pthread_cond_t *pcond);
volatile bool quit = false;

class ThreadData
{
public:
    ThreadData(const string &name, func_t func, pthread_mutex_t *pmtx, pthread_cond_t *pcond) 
    : name_(name), func_(func), pmtx_(pmtx), pcond_(pcond)
    {
    }

public:
    string name_;
    func_t func_;
    pthread_mutex_t *pmtx_;
    pthread_cond_t *pcond_;
};

void func1(const string &name, pthread_mutex_t *pmtx, pthread_cond_t *pcond)
{
    while (!quit)
    {
        // pthread_cond_wait一定是在加锁和解锁之间进行等待
        pthread_mutex_lock(pmtx);
        pthread_cond_wait(pcond, pmtx);//当代码执行时，当前线程会被立即阻塞
        cout << name << " running -- 抓取" << endl;
        pthread_mutex_unlock(pmtx);
    }
}

void func2(const string &name, pthread_mutex_t *pmtx, pthread_cond_t *pcond)
{
    while (!quit)
    {
        pthread_mutex_lock(pmtx);
        pthread_cond_wait(pcond, pmtx);//当代码执行时，当前线程会被立即阻塞
        cout << name << " running -- 识别" << endl;
        pthread_mutex_unlock(pmtx);
    }
}

void func3(const string &name, pthread_mutex_t *pmtx, pthread_cond_t *pcond)
{
    while (!quit)
    {
        pthread_mutex_lock(pmtx);
        pthread_cond_wait(pcond, pmtx);//当代码执行时，当前线程会被立即阻塞
        cout << name << " running -- 提取" << endl;
        pthread_mutex_unlock(pmtx);
    }
}

void func4(const string &name, pthread_mutex_t *pmtx, pthread_cond_t *pcond)
{
    while (!quit)
    {
        pthread_mutex_lock(pmtx);
        pthread_cond_wait(pcond, pmtx);//当代码执行时，当前线程会被立即阻塞
        cout << name << " running -- 比对" << endl;
        pthread_mutex_unlock(pmtx);
    }
}

void *Entry(void *args)
{
    ThreadData *td = (ThreadData*)args;//td在每一个线程自己私有的栈空间中保存
    td->func_(td->name_, td->pmtx_, td->pcond_);//这是一个函数，调用完成就要返回
    delete td;
    return nullptr;
}

int main()
{
    pthread_mutex_t mtx;
    pthread_cond_t cond;//条件变量
    pthread_mutex_init(&mtx, nullptr);
    pthread_cond_init(&cond, nullptr);

    pthread_t tids[TNUM];
    func_t funcs[TNUM] = {func1, func2, func3, func4};
    for (int i = 0; i < TNUM; i++)
    {
        string name = "Thread ";
        name += to_string(i + 1);
        ThreadData *td = new ThreadData(name, funcs[i], &mtx, &cond);
        pthread_create(tids + i, nullptr, Entry, (void *)td);
    }

    sleep(5);

    //唤醒线程(控制线程)
    int cnt = 10;
    while(cnt)
    {
        cout << "resume thread code run ... " << cnt-- << endl;
        //pthread_cond_signal(&cond);//一次唤醒一个线程
        pthread_cond_broadcast(&cond);//一次唤醒一批线程
        sleep(1);
    }

    cout << "控制结束" << endl;
    quit = true;
    pthread_cond_broadcast(&cond);//线程还在等待需要再次唤醒，之后结束线程

    for(int i = 0; i < TNUM; i++)
    {
        pthread_join(tids[i], nullptr);
        cout << "thread: " << tids[i] << "quit" << endl;
    }

    pthread_mutex_destroy(&mtx);
    pthread_cond_destroy(&cond);

    return 0;
}