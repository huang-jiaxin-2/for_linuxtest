#include <iostream>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>


void myperror(const char * msg)
{
  fprintf(stderr,"%s: %s\n", msg, strerror(errno));
}

int main()
{
  int fd = open("log.txt", O_RDONLY);
  if(fd < 0)
  {
    myperror("open");
    return 1;
  }


  close(fd);
  return 0;
}

