#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

int g_unval;
int g_val = 100;

int main(int argc, char* argv[], char* env[])
{
  //字面常量
//  "hello";
//  10;
//  30;//可以编译成功

  char* str = "helloworld";

  printf("code addr: %p\n",main);
  printf("init global addr: %p\n",&g_val);
  printf("uninit global addr: %p\n",&g_unval);

  static int test_num = 10;
  char* heap_mem = (char*)malloc(10);
  char* heap_mem1 = (char*)malloc(10);
  char* heap_mem2 = (char*)malloc(10);
  char* heap_mem3 = (char*)malloc(10);
  printf("heap addr: %p\n",heap_mem);
  printf("heapl addr: %p\n",heap_mem1);
  printf("heap2 addr: %p\n",heap_mem2);
  printf("heap3 addr: %p\n",heap_mem3);

  printf("test_num static stack addr: %p\n",&test_num);
  printf("stack addr: %p\n",&heap_mem);
  printf("stack1 addr: %p\n",&heap_mem1);
  printf("stack2 addr: %p\n",&heap_mem2);
  printf("stack3 addr: %p\n",&heap_mem3);

  printf("read only string addr: %p\n",str);
  int i = 0;
  for(i = 0; i < argc; i++)
  {
    printf("argv[%d]: %p\n", i, argv[i]);
  }
  for(i = 0; env[i]; i++)
  {
    printf("env[%d]: %p\n", i, env[i]);
  }

  return 0;
}
