#pragma once

#include <iostream>
#include <string>
#include <cstring>
#include <strings.h>
#include <assert.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <pthread.h>
#include <sys/wait.h>
#include "log.hpp"

static void service(int sock, const std::string &clientip, const uint16_t &clientport)
{
    char buffer[1024];
    while (true)
    {
        ssize_t s = read(sock, buffer, sizeof(buffer) - 1);
        if (s > 0)
        {
            buffer[s] = 0;
            std::cout << clientip << ":" << clientport << "# " << buffer << std::endl;
        }
        else if (s == 0) // 对端关闭连接
        {
            logMessage(NORMAL, "%s:%d shutdown, me too", clientip.c_str(), clientport);
            break;
        }
        else
        {
            logMessage(ERROR, "read socket error, %d:%s", errno, strerror(errno));
            break;
        }
        
        write(sock, buffer, strlen(buffer));
    }
    close(sock);
}

class ThreadDate
{
public:
    int _sock;
    std::string _ip;
    uint16_t _port;
};

class Tcp_Server
{
private:
    const static int gbacklog = 20;
    static void *threadRoutine(void *args)//必须得是static，因为不是static的话会多出一个this指针
    {
        pthread_detach(pthread_self());//线程分离
        ThreadDate *td = static_cast<ThreadDate*> (args);
        service(td->_sock, td->_ip, td->_port);
        delete td;
        
        return nullptr;
    }

public:
    Tcp_Server(uint16_t port, std::string ip = "")
        : _port(port), _ip(ip), listensock(-1)
    {
    }
    ~Tcp_Server()
    {
    }

    void InitServer()
    {
        // 1.创建socket
        listensock = socket(AF_INET, SOCK_STREAM, 0);
        if (listensock < 0)
        {
            //创建socket失败
            logMessage(FATAL, "create socket error, %d:%S", errno, strerror(errno));
            exit(2);
        }
        logMessage(NORMAL, "create socket success, listensock:%d", listensock);

        // 2.bind
        struct sockaddr_in local;
        memset(&local, 0, sizeof local);
        local.sin_family = AF_INET;
        local.sin_port = htons(_port);
        local.sin_addr.s_addr = _ip.empty() ? INADDR_ANY : inet_addr(_ip.c_str()); //INADDR_ANY：任意IP
        if (bind(listensock, (struct sockaddr *)&local, sizeof(local)) < 0)
        {
            //bind失败
            logMessage(FATAL, "bind error, %d:%s", errno, strerror(errno));
            exit(3);
        }

        // 3.建立连接
        if (listen(listensock, gbacklog) < 0)
        {
            //建立连接失败
            logMessage(FATAL, "listen error, %d:%s", errno, strerror(errno));
            exit(4);
        }

        logMessage(NORMAL, "init server success");
    }

    void Start()
    {
        // signal(SIGCHLD, SIG_IGN);//多进程版
        while (true)
        {
            // 4.获取连接
            struct sockaddr_in src;
            socklen_t len = sizeof(src);
            int servicesock = accept(listensock, (struct sockaddr *)&src, &len);
            // 获取连接失败
            if (servicesock < 0)
            {
                logMessage(ERROR, "accept error, %d:%s", errno, strerror(errno));
                continue;
            }
            // 获取连接成功
            uint16_t client_port = ntohs(src.sin_port);
            std::string client_ip = inet_ntoa(src.sin_addr);
            logMessage(NORMAL, "link success, servicesock: %d | %s : %d |\n", servicesock, client_ip.c_str(), client_port);

            // 5.开始通信
            // 版本1——单进程循环版
            // 缺点：一次只能处理一个客户端，处理完后才能处理下一个
            // service(servicesock, client_ip, client_port);
            // close(servicesock);

            //版本2.0——多进程（创建子进程）
            // pid_t id = fork();
            // assert(id != -1);
            // if(id == 0)
            // {
                //子进程
                // close(listensock);
                // service(servicesock, client_ip, client_port);
                // exit(0);
            // }
            //父进程
            // close(servicesock);

            //版本2.1——多进程版本（孤儿进程）
            // pid_t id = fork();
            // assert(id != -1);
            // if(id == 0)
            // {
                //子进程
                // close(listensock);
                // if(fork() > 0) exit(0);//子进程退出，由操作系统领养
                //孙子进程
                // service(servicesock, client_ip, client_port);
                // exit(0);
            // }
            //父进程
            // waitpid(id, nullptr, 0);
            // close(servicesock);

            //版本3——多线程版
            ThreadDate *td = new ThreadDate();
            td->_sock = servicesock;
            td->_ip = client_ip;
            td->_port = client_port;
            pthread_t tid;
            pthread_create(&tid, nullptr, threadRoutine, td);
        }
    }

private:
    uint16_t _port;
    std::string _ip;
    int listensock;
};