#ifndef _UDP_SERVER_HPP
#define _UDP_SERVER_HPP

#include <iostream>
#include <cstdio>
#include <string>
#include <cstring>
#include <strings.h>
#include <cstdlib>
#include <cerrno>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include "log.hpp"

#define SIZE 1024

class UdpServer
{
public:
    UdpServer(uint16_t port, std::string ip = "") : _port(port), _ip(ip), _sock(-1)
    {
    }
    bool InitServer()
    {
        // 创建套接字
        _sock = socket(AF_INET, SOCK_DGRAM, 0);
        if (_sock < 0)
        {
            logMessage(FATAL, "%d:%s", errno, strerror(errno));
            exit(2);
        }

        // bind
        struct sockaddr_in local;
        bzero(&local, sizeof(local)); // 清零
        local.sin_family = AF_INET;
        local.sin_port = htons(_port);                  // 主机转网络
        local.sin_addr.s_addr = _ip.empty() ? INADDR_ANY : inet_addr(_ip.c_str()); // 将ip转为点分十进制形式

        if (bind(_sock, (struct sockaddr *)&local, sizeof(local)) < 0)
        {
            logMessage(FATAL, "%d:%s", errno, strerror(errno));
            exit(2);
        }

        logMessage(NORMAL, "init udp server finish ... %s", strerror(errno));
        return true;
    }
    void StartServer()
    {
        char buffer[SIZE];
        for(;;)
        {
            //读取数据
            struct sockaddr_in peer;
            bzero(&peer, sizeof(peer));
            socklen_t len = sizeof(peer);
            ssize_t s = recvfrom(_sock, buffer, sizeof(buffer) - 1, 0, (struct sockaddr*)&peer, &len);
            if(s > 0)
            {
                buffer[s] = 0;
                uint16_t cli_port = ntohs(peer.sin_port);
                std::string cli_ip = inet_ntoa(peer.sin_addr);//将网络序列IP->字符串风格的IP
                printf("[%s:%d]#%s\n", cli_ip.c_str(), cli_port, buffer);
            }
            //写回数据
            sendto(_sock, buffer, strlen(buffer), 0, (struct sockaddr*)&peer, len);
        }
    }
    ~UdpServer() {
        if(_sock >= 0)
        {
            close(_sock);
        }
    }

private:
    uint16_t _port;  // 端口号
    std::string _ip; // ip地址
    int _sock;       // 套接字描述符
};

#endif