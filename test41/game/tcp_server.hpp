#pragma once

#include <iostream>
#include <string>
#include <cstring>
#include <strings.h>
#include <assert.h>
#include <unistd.h>
#include <signal.h>
#include <memory>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <pthread.h>
#include <sys/wait.h>
#include "threadPool/log.hpp"
#include "threadPool/thread.hpp"
#include "threadPool/threadPool.hpp"
#include "threadPool/Task.hpp"
#include "game.h"

static void play(int sock, const std::string &clientip, const uint16_t &clientport, const std::string &thread_name)
{
    char buffer[1024];
    while (true)
    {
        ssize_t s = read(sock, buffer, sizeof(buffer) - 1);
        std::string message;
        if (s > 0)
        {
            buffer[s] = 0;
            std::cout << thread_name << "|" << clientip << ":" << clientport << "# " << buffer << std::endl;
            if (strcmp(buffer, "start") == 0)
            {
                message = "玩家请开始游戏";
            }
            if (strcmp(buffer, "quit") == 0)
            {
               message = "玩家退出游戏成功";
               
            }
            write(sock, message.c_str(), message.size());
        }
        else if (s == 0) // 对端关闭连接
        {
            logMessage(NORMAL, "%s:%d 玩家已退出游戏,我也退出", clientip.c_str(), clientport);
            break;
        }
        else
        {
            logMessage(ERROR, "read socket error, %d:%s", errno, strerror(errno));
            break;
        }
    }
    close(sock);
}

class Tcp_Server
{
private:
    const static int gbacklog = 20;

public:
    Tcp_Server(uint16_t port, std::string ip = "")
        : _port(port), _ip(ip), listensock(-1), _threadpool_ptr(ThreadPool<Task>::getThreadPool())
    {
    }
    ~Tcp_Server()
    {
    }

    void InitServer()
    {
        // 1.创建socket
        listensock = socket(AF_INET, SOCK_STREAM, 0);
        if (listensock < 0)
        {
            // 创建socket失败
            logMessage(FATAL, "create socket error, %d:%S", errno, strerror(errno));
            exit(2);
        }
        logMessage(NORMAL, "create socket success, listensock:%d", listensock);

        // 2.bind
        struct sockaddr_in local;
        memset(&local, 0, sizeof local);
        local.sin_family = AF_INET;
        local.sin_port = htons(_port);
        local.sin_addr.s_addr = _ip.empty() ? INADDR_ANY : inet_addr(_ip.c_str()); // INADDR_ANY：任意IP
        if (bind(listensock, (struct sockaddr *)&local, sizeof(local)) < 0)
        {
            // bind失败
            logMessage(FATAL, "bind error, %d:%s", errno, strerror(errno));
            exit(3);
        }

        // 3.建立连接
        if (listen(listensock, gbacklog) < 0)
        {
            // 建立连接失败
            logMessage(FATAL, "listen error, %d:%s", errno, strerror(errno));
            exit(4);
        }

        logMessage(NORMAL, "init server success");
    }

    void Start()
    {
        _threadpool_ptr->run();
        while (true)
        {
            // 4.获取连接
            struct sockaddr_in src;
            socklen_t len = sizeof(src);
            int servicesock = accept(listensock, (struct sockaddr *)&src, &len);
            // 获取连接失败
            if (servicesock < 0)
            {
                logMessage(ERROR, "accept error, %d:%s", errno, strerror(errno));
                continue;
            }
            // 获取连接成功
            uint16_t client_port = ntohs(src.sin_port);
            std::string client_ip = inet_ntoa(src.sin_addr);
            logMessage(NORMAL, "link success, servicesock: %d | %s : %d |\n", servicesock, client_ip.c_str(), client_port);

            // 5.开始通信
            // 线程池版
            Task t(servicesock, client_ip, client_port, play);
            _threadpool_ptr->pushTask(t);
        }
    }

private:
    uint16_t _port;
    std::string _ip;
    int listensock;
    std::unique_ptr<ThreadPool<Task>> _threadpool_ptr;
};