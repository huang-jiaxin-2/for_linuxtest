#include <iostream>
#include <string>
#include <cstring>
#include <cstdio>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include "game.h"

void menu()
{
    printf("********************************\n");
    printf("*********  1. play     *********\n");
    printf("*********  0. exit     *********\n");
    printf("********************************\n");
}

void game()
{
    char ret = 0;
    // 存放下棋的数据
    char board[ROW][COL] = {0};
    // 初始化棋盘为全空格
    InitBoard(board, ROW, COL);
    // 打印棋盘
    DisplayBoard(board, ROW, COL);
    while (1)
    {
        // 玩家下棋
        player_move(board, ROW, COL);
        DisplayBoard(board, ROW, COL);
        // 判断输赢
        ret = is_win(board, ROW, COL);
        if (ret != 'C')
        {
            break;
        }
        // 电脑下棋
        computer_move(board, ROW, COL);// 随机下棋
        DisplayBoard(board, ROW, COL);
        ret = is_win(board, ROW, COL);
        if (ret != 'C')
        {
            break;
        }
    }
    if (ret == '*')
    {
        printf("恭喜你赢了\n");
    }
    else if (ret == '#')
    {
        printf("很遗憾你输了\n");
    }
    else
    {
        printf("平局\n");
    }
}

void Usage(std::string proc)
{
    std::cout << "\nUsage: " << proc << " serverIp serverPort\n"
              << std::endl;
}

void Print(int &sock, std::string &line)
{
    ssize_t s = send(sock, line.c_str(), line.size(), 0);
    if (s > 0)
    {
        char buffer[1024];
        ssize_t s = recv(sock, buffer, sizeof(buffer) - 1, 0);
        if (s > 0)
        {
            buffer[s] = 0;
            std::cout << "server 回应# " << buffer << std::endl;
        }
        else if (s == 0)
        {
            close(sock);
        }
    }
    else
    {
        close(sock);
    }
}

int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        Usage(argv[0]);
        exit(1);
    }
    std::string serverip = argv[1];
    uint16_t serverport = atoi(argv[2]);

    int sock = 0;
    std::string line;

    sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock < 0)
    {
        std::cerr << "socket error" << std::endl;
        exit(2);
    }
    struct sockaddr_in server;
    memset(&server, 0, sizeof(server));
    server.sin_family = AF_INET;
    server.sin_port = htons(serverport);
    server.sin_addr.s_addr = inet_addr(serverip.c_str());

    if (connect(sock, (struct sockaddr *)&server, sizeof(server)) < 0) // 连接服务器
    {
        std::cerr << "connect error" << std::endl;
        exit(3);
    }

    std::cout << "connect success" << std::endl;

    int input = 0;
    srand((unsigned int)time(NULL));
    do
    {
        menu();
        printf("请选择:>");
        scanf("%d", &input);
        switch (input)
        {
        case 1:
            line = "start";
            Print(sock, line);
            game(); // 游戏
            break;
        case 0:
            line = "quit";
            Print(sock, line);
            break;
        default:
            printf("选择错误\n");
            break;
        }
    } while (input);
    if (line == "quit")
    {
        exit(0);
    }

    return 0;
}