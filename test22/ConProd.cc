#include "BlockQueue.hpp"
#include "Task.hpp"
#include <pthread.h>
#include <string>
#include <unistd.h>
#include <assert.h>
#include <ctime>

std::string a[] = {"+", "-", "*", "/"};
int Select = 0;

// 消费
void *consumer(void *args)
{
    BlockQueue<Task> *bqueue = (BlockQueue<Task> *)args;
    while (true)
    {
        // int a;
        // 获取任务
        Task t;
        bqueue->pop(&t);
        // 完成任务
        std::cout << pthread_self() << " consume: " << t.x_ << a[Select] << t.y_ << "=" << t() << std::endl;
        sleep(1);
    }
    return nullptr;
}

// 生产
void *productor(void *args)
{
    BlockQueue<Task> *bqueue = (BlockQueue<Task> *)args;
    func_t arr[] = {myAdd, myDiv, myMul, mySub};
    while (true)
    {
        // 派发任务
        int x = rand() % 10 + 1;
        usleep(rand() % 1000);
        int y = rand() % 5 + 1;
        // int x, y;
        // std::cout << "Please Enter x:";
        // std::cin >> x;
        // std::cout << "Please Enter y:";
        // std::cin >> y;
        // std::cout << "Please Enter select[0-3]:";
        // std::cin >> select;
        //assert(select >= 0 && select <= 3);
        Task t(x, y, arr[Select]);
        // 生产
        bqueue->push(t);
        // 输出消息
        std::cout << pthread_self() << " productor: " << t.x_ << a[Select] << t.y_ << "=?" << std::endl;
        sleep(1);
    }
    return nullptr;
}

int main()
{
    srand((uint64_t)time(nullptr) ^ getpid() ^ 0x32434);
    BlockQueue<Task> *bqueue = new BlockQueue<Task>();
    pthread_t c[2], p[2];
    pthread_create(c, nullptr, consumer, bqueue);
    pthread_create(c + 1, nullptr, consumer, bqueue);
    pthread_create(p, nullptr, productor, bqueue);
    pthread_create(p + 1, nullptr, productor, bqueue);

    pthread_join(c[0], nullptr);
    pthread_join(c[1], nullptr);
    pthread_join(p[0], nullptr);
    pthread_join(p[1], nullptr);

    delete bqueue;
    return 0;
}