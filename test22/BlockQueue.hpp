#pragma once

#include <iostream>
#include "lockGuard.hpp"
#include <queue>
#include <pthread.h>

const int gDefultCap = 8;

template <class T>
class BlockQueue
{
private:
    bool isQueueEmpty()
    {
        return bq_.size() == 0;
    }
    bool isQueueFull()
    {
        return bq_.size() == capacity_;
    }

public:
    BlockQueue(int capacity = gDefultCap) : capacity_(capacity)
    {
        pthread_mutex_init(&mtx_, nullptr);
        pthread_cond_init(&Empty_, nullptr);
        pthread_cond_init(&Full_, nullptr);
    }

    // 生产者
    void push(const T &in)
    {
        // pthread_mutex_lock(&mtx_);
        // 检测当前临界资源是否满足条件
        // while (isQueueFull())
        // {
        // pthread_cond_wait(&Full_, &mtx_);
        // }
        // 访问临界资源
        // bq_.push(in);

        // 当生产到一定容量时在消费
        //  if (bq_.size() >= capacity_ / 2)
        //  {
        //  唤醒消费者
        //  pthread_cond_signal(&Empty_);
        // }
        // pthread_cond_signal(&Empty_);
        // pthread_mutex_unlock(&mtx_);

        lockGuard lockguard(&mtx_); // 自动调用构造函数
        while (isQueueFull())
        {
            pthread_cond_wait(&Full_, &mtx_);
        }
        // 访问临界资源
        bq_.push(in);
        pthread_cond_signal(&Empty_);

    } // 自动调用lockguard析构函数

    // 消费者
    void pop(T *out)
    {
        lockGuard lockguard(&mtx_);
        //pthread_mutex_lock(&mtx_);
        // 检测当前临界资源是否满足条件
        while (isQueueEmpty())
        {
            pthread_cond_wait(&Empty_, &mtx_);
        }
        // 访问临界资源
        *out = bq_.front();
        bq_.pop();

        // 当消费到一定容量时在生产
        //  if (bq_.size() < capacity_ / 2)
        //  {
        // 唤醒生产者
        //  pthread_cond_signal(&Full_);
        // }
        pthread_cond_signal(&Full_);
        //pthread_mutex_unlock(&mtx_);
    }

    ~BlockQueue()
    {
        pthread_mutex_destroy(&mtx_);
        pthread_cond_destroy(&Empty_);
        pthread_cond_destroy(&Full_);
    }

private:
    std::queue<T> bq_;     // 阻塞队列
    int capacity_;         // 容量上限
    pthread_mutex_t mtx_;  // 互斥锁保证队列安全
    pthread_cond_t Empty_; // 表示阻塞对列是否为空的条件
    pthread_cond_t Full_;  // 表示阻塞对列是否为满的条件
};