#include <iostream>
#include <cstdio>
#include <pthread.h>
#include <time.h>
#include <cassert>
#include <unistd.h>

using namespace std;

#define THREAD_NUM 10

class ThreadData
{
public:
    ThreadData(const string &n, pthread_mutex_t *pm)
        : tname(n), pmtx(pm)
    {
    }

public:
    string tname;
    pthread_mutex_t *pmtx;
};

// 加锁保护
// pthread_mutex_t mtx = PTHREAD_MUTEX_INITIALIZER; //全局和静态时使用

// 多线程进行抢票
int tickets = 10000; // 临界资源

void *getTicket(void *args)
{
    ThreadData *td = (ThreadData *)args;
    // 开始抢票
    while (true)
    {
        int n = pthread_mutex_lock(td->pmtx);
        assert(n == 0);
        // 临界区
        if (tickets > 0)
        {
            usleep(rand() % 1000);
            printf("%s: %d\n", td->tname.c_str(), tickets);
            tickets--;
            n = pthread_mutex_unlock(td->pmtx);
            assert(n == 0);
        }
        else
        {
            n = pthread_mutex_unlock(td->pmtx);
            assert(n == 0);

            break;
        }

        // 抢完票后
        usleep(rand() % 200000);
    }
    delete td;
    return nullptr;
}

int main()
{
    time_t start = time(nullptr);
    pthread_mutex_t mtx;
    pthread_mutex_init(&mtx, nullptr);
    srand((unsigned long)time(nullptr) ^ getpid() ^ 0x243);
    pthread_t t[THREAD_NUM];

    // pthread_create(&t1, nullptr, getTicket, (void*)"thread 1");
    // pthread_create(&t2, nullptr, getTicket, (void*)"thread 2");
    // pthread_create(&t3, nullptr, getTicket, (void*)"thread 3");

    for (int i = 0; i < THREAD_NUM; i++)
    {
        string name = "thread ";
        name += to_string(i + 1);

        ThreadData *td = new ThreadData(name, &mtx);
        pthread_create(t + i, nullptr, getTicket, (void *)td);
    }

    for (int i = 0; i < THREAD_NUM; i++)
    {
        pthread_join(t[i], nullptr);
    }

    pthread_mutex_destroy(&mtx);

    time_t end = time(nullptr);

    cout << "cast: " << (int)(end - start) << "秒" << endl;
    return 0;
}
