#pragma once

#include <iostream>
#include <queue>
#include <mutex>
#include <pthread.h>
#include "lockGuard.hpp"

#define Default_Capacity 5

template <class T>
class BlockQueue
{
private:
    bool isQueueFull()
    {
        return _bq.size() == _capacity;
    }
    bool isQueueEmpty()
    {
        return _bq.size() == 0;
    }

public:
    BlockQueue(int capacity = Default_Capacity) : _capacity(capacity)
    {
        pthread_mutex_init(&_mtx, nullptr);
        pthread_cond_init(&_Empty, nullptr);
        pthread_cond_init(&_Full, nullptr);
    }

    void push(const T &in)
    {

        lockGuard lockguard(&_mtx);
        // pthread_mutex_lock(&_mtx);
        while (isQueueFull())
            pthread_cond_wait(&_Full, &_mtx);
        _bq.push(in);
        if (_bq.size() >= _capacity / 2)
            pthread_cond_signal(&_Empty);
        // pthread_mutex_unlock(&_mtx);
    }

    void pop(T *out)
    {

        lockGuard lockguard(&_mtx);
        // pthread_mutex_lock(&_mtx);
        while (isQueueEmpty())
            pthread_cond_wait(&_Empty, &_mtx);
        *out = _bq.front();
        _bq.pop();
        if (_bq.size() <= 1)
            pthread_cond_signal(&_Full);
        // pthread_mutex_unlock(&_mtx);
    }

    ~BlockQueue()
    {
        pthread_mutex_destroy(&_mtx);
        pthread_cond_destroy(&_Empty);
        pthread_cond_destroy(&_Full);
    }

private:
    // 前面加_表示是类内成员
    std::queue<T> _bq;     // 阻塞队列
    int _capacity;         // 容量上限
    pthread_mutex_t _mtx;  // 加锁保护阻塞队列
    pthread_cond_t _Empty; // 用来表示阻塞队列是否为空
    pthread_cond_t _Full;  // 用来表示阻塞队列是否为满
};