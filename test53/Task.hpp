#pragma once

#include <iostream>
#include <functional>

class Task
{
    typedef std::function<int(int, int)> func_t;

public:
    Task() {}
    Task(int x, int y, func_t func) : _x(x), _y(y), _func(func)
    {
    }
    int operator()()
    {
        return _func(_x, _y);
    }
    
    int _x;
    int _y;
    func_t _func;
};
