#pragma once

#include <iostream>
#include <pthread.h>

class Mutex
{
public:
    Mutex(pthread_mutex_t *mtx) : _pmtx(mtx)
    {
    }
    void lock()
    {
        pthread_mutex_lock(_pmtx);
    }
    void unlock()
    {
        pthread_mutex_unlock(_pmtx);
    }
    ~Mutex()
    {
    }

private:
    pthread_mutex_t *_pmtx;
};

// RAII——资源创建即初始化
class lockGuard
{
public:
    lockGuard(pthread_mutex_t *mtx) : _mtx(mtx)
    {
        _mtx.lock();
    }
    ~lockGuard()
    {
        _mtx.unlock();
    }

private:
    Mutex _mtx;
};