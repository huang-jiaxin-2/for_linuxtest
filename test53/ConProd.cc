#include "BlockQueue.hpp"
#include "Task.hpp"
#include <pthread.h>
#include <unistd.h>
#include <ctime>

using std::cout;
using std::endl;

int MyAdd(int x, int y)
{
    return x + y;
}

// 消费者
void *consumer(void *args)
{
    BlockQueue<Task> *bqueue = (BlockQueue<Task> *)args;
    while (true)
    {
        // int a;
        // 获取任务
        Task t;
        bqueue->pop(&t);
        int result = 0;

        // 打印结果
        cout << "consumer:" << t._x << "+" << t._y << "=" << t() << endl;
        sleep(1);

        // cout << "消费一个数据:" << a << endl;
        // cout << endl;
    }
    return nullptr;
}

// 生产者
void *producter(void *args)
{
    BlockQueue<Task> *bqueue = (BlockQueue<Task> *)args;
    // int a = 1;
    while (true)
    {
        // 制作任务
        int x = rand() % 10 + 1;
        usleep(rand() % 1000);
        int y = rand() % 5 + 1;

        // 生产任务
        Task t(x, y, MyAdd);
        bqueue->push(t);

        // 打印消息
        cout << "producter:" << t._x << "+" << t._y << "=?" << endl;

        // cout << "生产一个数据:" << a << endl;
        // a++;
    }
    return nullptr;
}

int main()
{
    srand((uint64_t)time(nullptr) ^ getpid() ^ 0x2354528);
    BlockQueue<Task> *bqueue = new BlockQueue<Task>();
    pthread_t c, p;
    // 创建线程
    pthread_create(&c, nullptr, consumer, bqueue);
    pthread_create(&c, nullptr, producter, bqueue);

    // 回收线程资源
    pthread_join(c, nullptr);
    pthread_join(p, nullptr);

    return 0;
}