#include <iostream>
#include <cstdio>
#include <cstring>
#include <assert.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

using namespace std;

int main()
{
    //1.创建管道
    int pipefd[2] = {0};//pipefd[0]:读端，pipefd[1]:写端
    int n = pipe(pipefd);
    assert(n != -1);//在Debug下才有用
    (void)n;//确保在release不报警告

//条件编译
#ifdef DEBUG
    cout << "pipefd[0]:" << pipefd[0] << endl;
    cout << "pipefd[1]:" << pipefd[1] << endl;
#endif

    //2.创建子进程并构建通信信道——父进程写入，子进程读取
    pid_t id = fork();
    assert(id != -1);
    if(id == 0)
    {
        //子进程
        close(pipefd[1]);//关闭子进程不需要的fd

        //读取数据
        char buffer[1024];
        while(true)
        {
            //sleep(10);
            ssize_t s = read(pipefd[0], buffer, sizeof buffer - 1);
            if(s > 0)
            {
                buffer[s] = 0;
                cout << "child get a massage [" << getpid() << "] father say:" << buffer << endl;
            }
            // else if(s == 0)
            // {
                // cout << "writer quit, me quit too" << endl;
                // break;
            // }
        }

        close(pipefd[0]);
        exit(0);
    }
    //父进程
    close(pipefd[0]);//关闭父进程不需要的fd

    //发送数据
    string message = "我是父进程，我正在给你发送数据";
    int count = 0;//统计发送次数
    char send_buffer[1024];
    while(true)
    {
        //sleep(10);
        //将要发送的数据打入send_massage中
        snprintf(send_buffer, sizeof send_buffer, "%s[%d] : %d\n", message.c_str(), getpid(), count++);
        //写入数据
        write(pipefd[1], send_buffer, strlen(send_buffer));
        sleep(1);
        // cout << count << endl;
        // if(count == 5)
        // {
            // cout << "writer quit(father)" << endl;
            // break;
        // }
    }

    //等待子进程退出
    pid_t ret = waitpid(id, nullptr, 0);
    assert(ret > 0);
    (void)ret;
    close(pipefd[1]);

    return 0;
}
