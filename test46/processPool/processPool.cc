#include <iostream>
#include <vector>
#include <cstdio>
#include <unistd.h>
#include <stdlib.h>
#include <ctime>
#include <cassert>
#include <sys/wait.h>
#include <sys/types.h>
#include "Task.hpp"

#define PROCESS_NUM 5

using namespace std;

int WaitCommand(int waitFd, bool &quit)
{
    uint32_t command = 0;
    ssize_t s = read(waitFd, &command, sizeof command);
    if (s == 0)
    {
        quit = true;
        return -1;
    }
    assert(s == sizeof(uint32_t));
    return command;
}

void sendAndWakeup(pid_t who, int fd, uint32_t command)
{
    write(fd, &command, sizeof command);
    cout << "main process: call process " << who << " execute " << desc[command] << " through " << fd << endl;
}

int main()
{
    load();
    vector<pair<pid_t, int>> slots;
    // 创建多个进程
    for (int i = 0; i < PROCESS_NUM; i++)
    {
        // 创建管道
        int pipefd[2] = {0};
        int n = pipe(pipefd);
        assert(n == 0);
        (void)n;

        pid_t id = fork();
        assert(id != -1);

        // 子进程
        if (id == 0)
        {
            close(pipefd[1]); // 关闭写端
            while (true)
            {
                // 等命令——如果对方不发，就阻塞
                bool quit = false;
                int command = WaitCommand(pipefd[0], quit);
                if (quit)
                    break;
                // 执行相应的命令
                if (command >= 0 && command < handlerSize())
                {
                    callbacks[command]();
                }
                else
                {
                    cout << "非法command: " << command << endl;
                }
            }
            exit(1);
        }
        // 父进程
        close(pipefd[0]); // 关闭读端
        slots.push_back(pair<pid_t, int>(id, pipefd[1]));//进程id : 文件描述符fd

        // 派发任务
        // 生成一个随机数——随机数式的负载均衡
        srand((unsigned long)time(nullptr) ^ getpid() ^ 12543232345L);
        while (true)
        {

            //选择一个任务
            int command = rand() % handlerSize();
            //选择一个进程
            int choice = rand() % slots.size();
            //把任务给指定的进程
            sendAndWakeup(slots[choice].first, slots[choice].second, command);
            sleep(1);

            // int select;
            // int command;
            // cout << "###########################################" << endl;
            // cout << "#   1.show funcitions   2.send command    #" << endl;
            // cout << "###########################################" << endl;
            // cout << "please select> ";
            // cin >> select;
            // if (select == 1)
                // showHandler();
            // else if (select == 2)
            // {
                // cout << "select Your Command> ";
                //选择任务
                // cin >> command;
                //选择进程
                // int choice = rand() % slots.size();
                //把任务给指定的进程
                // sendAndWakeup(slots[choice].first, slots[choice].second, command);
                // sleep(1);
            // }
            // else
            // {
                // cout << "非法输入，请重新选择" << endl;
                // continue;
            // }
        }
    }

    // 关闭所有文件描述符
    for (const auto &slot : slots)
    {
        close(slot.second);
    }

    // 回收子进程的信息
    for (const auto &slot : slots)
    {
        waitpid(slot.first, nullptr, 0);
    }
}