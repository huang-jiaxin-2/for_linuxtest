#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>
#include <unistd.h>
#include <algorithm>
#include <functional>

typedef std::function<void()> func;

std::vector<func> callbacks;
std::unordered_map<int, std::string> desc;

void readMySQL()
{
    std::cout << "process[" << getpid() << "] 执行访问数据库的任务\n"
              << std::endl;
}

void execuleUrl()
{
    std::cout << "process[" << getpid() << "] 执行url解析\n"
              << std::endl;
}

void cal()
{
    std::cout << "process[" << getpid() << "] 执行加密任务\n"
              << std::endl;
}

void persist()
{
    std::cout << "process[" << getpid() << "] 执行数据持久化任务\n"
              << std::endl;
}

void load()
{
    desc.insert({callbacks.size(), "readMySQL: 读取数据库"});
    callbacks.push_back(readMySQL);

    desc.insert({callbacks.size(), "execuleUrl: 进行url解析"});
    callbacks.push_back(execuleUrl);

    desc.insert({callbacks.size(), "cal: 进行加密计算"});
    callbacks.push_back(cal);

    desc.insert({callbacks.size(), "persist: 进行数据的文件保存"});
    callbacks.push_back(persist);
}

bool cmp(std::pair<int, std::string>& l, std::pair<int, std::string>& r)
{
    // 如果key相等，比较value值
	if (l.first == r.first)
		return l.second < r.second;
	else
	// 否则比较key值
		return  l.first < r.first;
}


void showHandler()
{
    std::vector<std::pair<int, std::string>> ret(desc.begin(), desc.end());
    std::sort(ret.begin(), ret.end(), cmp);
    for (const auto &iter : ret)
    {
        std::cout << iter.first << "\t" << iter.second << std::endl;
    }
}

int handlerSize()
{
    return callbacks.size();
}
