#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

int main()
{
  while(1)
  {
    pid_t pid = getpid();//自己进程的pid
    pid_t ppid = getppid();//父进程
    printf("hello world,pid: %d, ppid: %d\n", pid, ppid);
    sleep(1);
  }
  return 0;
}
