#include <stdio.h>
#include <unistd.h>

int g_val = 100;

int main()
{
  pid_t id = fork();
  if(id == 0)
  {
    int cnt = 0;
    //child
    while(1)
    {
       printf("I am child. pid:%d ppid:%d g_val:%d &g_val:%p\n",getpid(), getppid(), g_val, &g_val); 
       sleep(1);
       cnt++;
       if(cnt == 5)
       {
        g_val = 200;
        printf("child change g_val 100 -> 200 success\n");
       }
    }
  }
  else
  { 

    while(1)
    {
       printf("I am father. pid:%d ppid:%d g_val:%d &g_val:%p\n",getpid(), getppid(), g_val, &g_val); 
       sleep(1);
    }
  }

  return 0;
}
