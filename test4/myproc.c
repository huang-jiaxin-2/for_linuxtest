#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

int main()
{
  pid_t id = fork();
  if(id < 0)
  {
    perror("fork");
    exit(1);//表示进程运行完毕，结果不正确
  }
  else if(id == 0)
  {
    //子进程
    int cnt = 5;
    while(cnt)
    {
      printf("cnt:%d, 我是子进程, pid:%d, ppid:%d\n",cnt, getpid(), getppid());
      sleep(1);
      cnt--;
    
    }
    exit(0);
  }
  else{

      printf("我是父进程, pid:%d, ppid:%d\n",getpid(), getppid());
      pid_t ret = wait(NULL);
      if(ret > 0)
      {
        printf("等待子进程成功,ret:%d\n",ret);
      }
      sleep(7);
      //下面进行对比演示
      //父进程
    while(1)
    {
      printf("我是父进程, pid:%d, ppid:%d\n",getpid(), getppid());
      sleep(1);
    }
  }

}

//int main()
//{
 // printf("hello world,pid:%d,ppid:%d\n",getpid(), getppid());
//  int number = 0;
//  for(number = 0; number < 150; number++)
//  {
//    printf("%d: %s\n",number, strerror(number));
//  }
 //return 10;
 // printf("hello world\n");
 // printf("hello world\n");
 // printf("hello world\n");
 // printf("hello world\n");
 // printf("hello world");
  
  //exit(111);
//  _exit(111);
//
//  printf("hjx\n");
//  printf("hjx\n");
//  printf("hjx\n");
//  printf("hjx\n");
//  printf("hjx\n");
//}
