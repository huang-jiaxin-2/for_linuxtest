#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

int g_unval;
int g_val = 10;

int main(int argc,char* argv[],char* env[])
{
  printf("code addr:%p\n",main);
  printf("Init global addr:%p\n",&g_val);
  printf("unInit global addr:%p\n",&g_unval);

  char* heap = (char*)malloc(10);
  char* heap1 = (char*)malloc(10);
  char* heap2 = (char*)malloc(10);
  char* heap3 = (char*)malloc(10);
  printf("heap addr:%p\n",heap);
  printf("heap1 addr:%p\n",heap1);
  printf("heap2 addr:%p\n",heap2);
  printf("heap3 addr:%p\n",heap3);

  printf("stack addr:%p\n",&heap);
  printf("stack1 addr:%p\n",&heap1);
  printf("stack2 addr:%p\n",&heap2);
  printf("stack3 addr:%p\n",&heap3);
  
  int i = 0;
  for(i = 0;i < argc; i++)
  {
    printf("argv[%d]:%p\n",i,argv[i]);
  }
  for(i = 0; env[i]; i++)
  {
    printf("env[%d]:%p\n",i,env[i]);
  }
  return 0;
}
