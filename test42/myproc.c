
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <unistd.h>

//code;
int main()
{
  pid_t id = fork();
  if(id < 0)
  {
    perror("fork");
    exit(0);//创建子进程失败
  }
  else if(id == 0)
  {
    //子进程
    int cnt = 5;
    while(cnt)
    {
      printf("cnt:%d,我是子进程,pid:%d,ppid:%d\n", cnt--, getpid(),getppid());
      sleep(1);
      //int* p =NULL;
      //*p = 100;
      }
   // code = 15;
    exit(100);//子进程退出
  }
  else 
  {
    //父进程
    int quit = 0;
    while(!quit)
    {
      int status = 0;
      pid_t res = waitpid(-1, 0, WNOHANG);
      if(res > 0)
      {
        //等待成功并且子进程退出
        printf("等待子进程退出成功,退出码:%d\n", WEXITSTATUS(status));
        quit = 1;
      }
      else if(res == 0)
      {
        //等待成功并且子进程还未退出
        printf("子进程还在运行,暂时还没有退出,父进程在等等,先处理其它事情吧!!\n");
      }
      else
      {
        //等待失败
        printf("wait失败\n");
        quit = 1;
      }
      sleep(1);
    }



    // printf("我是父进程,pid:%d,ppid:%d\n", getpid(), getppid());
    // sleep(7);
    // pid_t ret = wait(NULL);
    // int status = 0;
    // pid_t ret = waitpid(id, &status, 0);//阻塞式等待
    // if(ret > 0)
    // {
      // if(WIFEXITED(status))
      // {
        // printf("子进程执行完毕,子进程的退出码:%d\n", WEXITSTATUS(status));
      // }
      // else
      // {
        // printf("子进程异常退出:%d,退出信号为:%d\n", WIFEXITED(status), WTERMSIG(status));
      // }
    // }
   // if(ret > 0)
   // {
   //   printf("等待子进程成功,ret:%d,子进程收到的信号编号:%d,子进程的退出码:%d\n",ret, status & 0x7f, (status >> 8)&0xff);
   //   printf("code:%d\n",code);
   // }
   // while(1)
   // {
   //   printf("我是父进程,pid:%d,ppid:%d\n", getpid(), getppid());
   //   sleep(1);
   // }
  }

  return 0;
}






//int main()
//{
//  printf("hello world");
//  sleep(3);
//  _exit(22);
//}

//void Func()
//{
//  //打印1-10
//  for(int i = 1; i <= 10; i++)
//  {
//    printf("%d ", i);
//  }
//  printf("\n");
//  exit(22);
//}
//
//int main()
//{
//  printf("hello world\n");
//  printf("hello world\n");
//  Func();
//  printf("hello world\n");
//  exit(11);
//  printf("hello world\n");
//  return 0;
//}
