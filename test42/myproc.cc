#include <iostream>
#include <vector>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <unistd.h>

typedef void (*handler_t)(); //函数指针类型

std::vector<handler_t> handlers; //函数指针数组

void fun_one()
{
  int a = 10;
  int b = 20;
   printf("这是一个加法: %d + %d = %d\n", a, b, a + b);
}
void fun_two()
{
  int a = 10;
  int b = 20;
   printf("这是一个减法: %d - %d = %d\n", b, a, b - a);
}

// 设置对应的方法回调
void Load()
{
   handlers.push_back(fun_one);
   handlers.push_back(fun_two);
}


int main()
{
  pid_t id = fork();
  if(id < 0)
  {
    perror("fork");
    exit(0);//创建子进程失败
  }
  else if(id == 0)
  {
    //子进程
    int cnt = 5;
    while(cnt)
    {
      printf("cnt:%d,我是子进程,pid:%d,ppid:%d\n", cnt--, getpid(),getppid());
      sleep(1);
      }
    exit(100);//子进程退出
  }
  else 
  {
    //父进程
    int quit = 0;
    while(!quit)
    {
      int status = 0;
      pid_t res = waitpid(-1, 0, WNOHANG);
      if(res > 0)
      {
        //等待成功并且子进程退出
        printf("等待子进程退出成功,退出码:%d\n", WEXITSTATUS(status));
        quit = 1;
      }
      else if(res == 0)
      {
        //等待成功并且子进程还未退出
        printf("子进程还在运行,暂时还没有退出,父进程在等等,先处理其它事情吧!!\n");
        if(handlers.empty()) Load();
        for(auto iter : handlers)
        {
          //执行其它任务
          iter();
        }
      }
      else
      {
        //等待失败
        printf("wait失败\n");
        quit = 1;
      }
      sleep(1);
    }
  }
  return 0;
}

