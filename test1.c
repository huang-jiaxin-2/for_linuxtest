#include <stdio.h>
#include <string.h>
#include <unistd.h>
#define NUM 102
int main()
{
  char bar[NUM];
  memset(bar,0,sizeof(bar));
  int n=0;
  const char* lable="|/-\\";
  while(n<=100) 
  {
    printf("[%-100s [%d%%] %c\r",bar,n, lable[n%4]);
    bar[n++] = '#';
    fflush(stdout);
    usleep(30000);
  }
  printf("\n");
  return 0;
}
