#pragma once

#include <iostream>
#include <functional>

typedef std::function<int(int, int)> func_t;

int myAdd(int x, int y)
{
    return x + y;
}
int mySub(int x, int y)
{
    return x - y;
}
int myMul(int x, int y)
{
    return x * y;
}
int myDiv(int x, int y)
{
    return x / y;
}

class Task
{
public:
    Task() {}
    Task(int x, int y, func_t func) : x_(x), y_(y), func_(func)
    {
    }
    int operator()()
    {
        return func_(x_, y_);
    }
public:
    int x_;
    int y_;
    func_t func_;
};