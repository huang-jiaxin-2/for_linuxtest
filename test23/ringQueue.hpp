#ifndef _RING_QUEUE_HPP_
#define _RING_QUEUE_HPP_

#include <iostream>
#include <vector>
#include <pthread.h>
#include "sem.hpp"

const int g_default_num = 5;

template <class T>
class RingQueue
{
public:
    RingQueue(int default_num = g_default_num)
        : ring_queue_(default_num)
        , num_(default_num)
        , c_step_(0)
        , p_step_(0)
        , space_sem_(default_num)
        , data_sem_(0)
    {
        pthread_mutex_init(&clock_, nullptr);
        pthread_mutex_init(&plock_, nullptr);
    }
    ~RingQueue() {}
    //生产者:空间资源, 临界资源：下标
    void push(const T &in)
    {
        space_sem_.p();
        pthread_mutex_lock(&plock_);
        ring_queue_[p_step_++] = in;
        p_step_ %= num_;
        pthread_mutex_unlock(&plock_);
        data_sem_.v();
    }
    //消费者:数据资源, 临界资源：下标
    void pop(T *out)
    {
        data_sem_.p();
        pthread_mutex_lock(&clock_);
        *out = ring_queue_[c_step_++];
        c_step_ %= num_;
        pthread_mutex_unlock(&clock_);
        space_sem_.v();
    }
    void debug()
    {
        std::cerr << "size: " << ring_queue_.size() << " num: " << num_ << std::endl;
    }

private:
    std::vector<T> ring_queue_;
    int num_;
    int c_step_;//消费者下标
    int p_step_;//生产者下标
    Sem space_sem_;
    Sem data_sem_;
    pthread_mutex_t clock_;
    pthread_mutex_t plock_;
};

#endif