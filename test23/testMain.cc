#include "ringQueue.hpp"
#include "Task.hpp"
#include <cstdlib>
#include <string>
#include <time.h>
#include <sys/wait.h>
#include <unistd.h>

std::string a[] = {"+", "-", "*", "/"};
int Select = 0;

void *consumer(void *args)
{
    RingQueue<Task> *rq = (RingQueue<Task> *)args;
    while (true)
    {
        // int x;
        //  1.从环形队列中获取任务或者数据
        // rq->pop(&x);
        //  2.进行一定的处理
        // std::cout << "消费：" << x << "[" << pthread_self() << "]" << std::endl;
        //  sleep(1);

        Task t;
        rq->pop(&t);
        std::cout << pthread_self() << " consume: " << t.x_ << a[Select] << t.y_ << "=" << t() << std::endl;
    }
    return nullptr;
}

void *productor(void *args)
{
    RingQueue<Task> *rq = (RingQueue<Task> *)args;
    while (true)
    {
        // sleep(1);
        // 1.构建数据或任务对象
        // int x = rand() % 100 + 1;
        // std::cout << "生产：" << x << "[" << pthread_self() << "]" << std::endl;
        // 2.推送到环形队列中
        // rq->push(x);
        func_t arr[] = {myAdd, myDiv, myMul, mySub};
        int x = rand() % 10 + 1;
        usleep(rand() % 1000);
        int y = rand() % 5 + 1;
        Task t(x, y, arr[Select]);
        // 生产
        rq->push(t);
        // 输出消息
        std::cout << pthread_self() << " productor: " << t.x_ << a[Select] << t.y_ << "=?" << std::endl;
        sleep(1);
    }
    return nullptr;
}

int main()
{
    srand((uint64_t)time(nullptr) ^ getpid());
    RingQueue<Task> *rq = new RingQueue<Task>();
    // rq->debug();
    pthread_t c[3], p[3];
    for (int i = 0; i < 3; i++)
    {
        pthread_create(c + i, nullptr, consumer, (void *)rq);
        pthread_create(p + i, nullptr, productor, (void *)rq);
    }

    for (int i = 0; i < 3; i++)
    {
        pthread_join(c[i], nullptr);
        pthread_join(p[i], nullptr);
    }

    return 0;
}