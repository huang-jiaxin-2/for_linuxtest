-- MySQL dump 10.13  Distrib 5.7.43, for Linux (x86_64)
--
-- Host: localhost    Database: hjx
-- ------------------------------------------------------
-- Server version	5.7.43

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `hjx`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `hjx` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `hjx`;

--
-- Table structure for table `class_tb`
--

DROP TABLE IF EXISTS `class_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class_tb` (
  `id` bigint(20) NOT NULL,
  `name` varchar(32) NOT NULL,
  `teacher` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `class_tb`
--

LOCK TABLES `class_tb` WRITE;
/*!40000 ALTER TABLE `class_tb` DISABLE KEYS */;
INSERT INTO `class_tb` VALUES (105,'C++','航哥');
/*!40000 ALTER TABLE `class_tb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `myclass`
--

DROP TABLE IF EXISTS `myclass`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `myclass` (
  `class_name` varchar(20) NOT NULL,
  `class_room` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `myclass`
--

LOCK TABLES `myclass` WRITE;
/*!40000 ALTER TABLE `myclass` DISABLE KEYS */;
INSERT INTO `myclass` VALUES ('7_class','208教室'),('6_class','207教室'),('5_class','');
/*!40000 ALTER TABLE `myclass` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stu_tb`
--

DROP TABLE IF EXISTS `stu_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stu_tb` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `class_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `class_id` (`class_id`),
  CONSTRAINT `stu_tb_ibfk_1` FOREIGN KEY (`class_id`) REFERENCES `class_tb` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stu_tb`
--

LOCK TABLES `stu_tb` WRITE;
/*!40000 ALTER TABLE `stu_tb` DISABLE KEYS */;
INSERT INTO `stu_tb` VALUES (3,'王五',105);
/*!40000 ALTER TABLE `stu_tb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stus`
--

DROP TABLE IF EXISTS `stus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stus` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `qq` varchar(32) DEFAULT NULL COMMENT '唯一键',
  `tel` varchar(16) DEFAULT NULL COMMENT '唯一键',
  PRIMARY KEY (`id`),
  UNIQUE KEY `qq` (`qq`),
  UNIQUE KEY `tel` (`tel`)
) ENGINE=InnoDB AUTO_INCREMENT=1006 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stus`
--

LOCK TABLES `stus` WRITE;
/*!40000 ALTER TABLE `stus` DISABLE KEYS */;
INSERT INTO `stus` VALUES (1000,'张三','32434@qq.com','54323'),(1002,'李无','332264324@qq.com','543332623'),(1003,'李四','322434@qq.com','543223'),(1004,'李无','3322434@qq.com','5433223'),(1005,'李无','33224324@qq.com','54333223');
/*!40000 ALTER TABLE `stus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t1`
--

DROP TABLE IF EXISTS `t1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t1` (
  `num` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t1`
--

LOCK TABLES `t1` WRITE;
/*!40000 ALTER TABLE `t1` DISABLE KEYS */;
INSERT INTO `t1` VALUES (127),(-128),(-1),(1);
/*!40000 ALTER TABLE `t1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t10`
--

DROP TABLE IF EXISTS `t10`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t10` (
  `name` varchar(21844) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t10`
--

LOCK TABLES `t10` WRITE;
/*!40000 ALTER TABLE `t10` DISABLE KEYS */;
/*!40000 ALTER TABLE `t10` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t11`
--

DROP TABLE IF EXISTS `t11`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t11` (
  `t1` date DEFAULT NULL,
  `t2` datetime DEFAULT NULL,
  `t3` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t11`
--

LOCK TABLES `t11` WRITE;
/*!40000 ALTER TABLE `t11` DISABLE KEYS */;
INSERT INTO `t11` VALUES ('1990-01-01','2023-04-09 10:01:31','2023-10-13 12:12:49');
/*!40000 ALTER TABLE `t11` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t12`
--

DROP TABLE IF EXISTS `t12`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t12` (
  `name` varchar(20) NOT NULL,
  `age` tinyint(3) unsigned DEFAULT '18',
  `gender` char(2) DEFAULT '男'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t12`
--

LOCK TABLES `t12` WRITE;
/*!40000 ALTER TABLE `t12` DISABLE KEYS */;
INSERT INTO `t12` VALUES ('张三',18,'男'),('张三',22,'男'),('李四',21,'女');
/*!40000 ALTER TABLE `t12` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t13`
--

DROP TABLE IF EXISTS `t13`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t13` (
  `name` varchar(20) NOT NULL,
  `age` int(11) NOT NULL DEFAULT '18'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t13`
--

LOCK TABLES `t13` WRITE;
/*!40000 ALTER TABLE `t13` DISABLE KEYS */;
INSERT INTO `t13` VALUES ('fdfd',100);
/*!40000 ALTER TABLE `t13` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t14`
--

DROP TABLE IF EXISTS `t14`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t14` (
  `name` varchar(20) NOT NULL,
  `age` tinyint(4) DEFAULT '18' COMMENT '年龄',
  `gender` char(1) NOT NULL DEFAULT '男' COMMENT '性别'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t14`
--

LOCK TABLES `t14` WRITE;
/*!40000 ALTER TABLE `t14` DISABLE KEYS */;
INSERT INTO `t14` VALUES ('fdfgf',19,'男'),('fdfgf',18,'男'),('fdfgf',NULL,'男');
/*!40000 ALTER TABLE `t14` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t15`
--

DROP TABLE IF EXISTS `t15`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t15` (
  `a` int(9) unsigned zerofill DEFAULT NULL,
  `b` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t15`
--

LOCK TABLES `t15` WRITE;
/*!40000 ALTER TABLE `t15` DISABLE KEYS */;
INSERT INTO `t15` VALUES (000000001,2);
/*!40000 ALTER TABLE `t15` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t16`
--

DROP TABLE IF EXISTS `t16`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t16` (
  `id` int(10) unsigned NOT NULL COMMENT '学生的学号是主键',
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t16`
--

LOCK TABLES `t16` WRITE;
/*!40000 ALTER TABLE `t16` DISABLE KEYS */;
INSERT INTO `t16` VALUES (1,'张三');
/*!40000 ALTER TABLE `t16` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t17`
--

DROP TABLE IF EXISTS `t17`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t17` (
  `id` varchar(20) NOT NULL,
  `course` varchar(20) NOT NULL COMMENT '课程号',
  `score` tinyint(3) unsigned DEFAULT '60' COMMENT '成绩',
  PRIMARY KEY (`id`,`course`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t17`
--

LOCK TABLES `t17` WRITE;
/*!40000 ALTER TABLE `t17` DISABLE KEYS */;
INSERT INTO `t17` VALUES ('001','003',66),('001','004',76),('002','003',76);
/*!40000 ALTER TABLE `t17` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t18`
--

DROP TABLE IF EXISTS `t18`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t18` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t18`
--

LOCK TABLES `t18` WRITE;
/*!40000 ALTER TABLE `t18` DISABLE KEYS */;
INSERT INTO `t18` VALUES (1,'张三'),(2,'张三'),(3,'张三'),(4,'张三'),(5,'张三'),(12,'李四'),(14,'李四');
/*!40000 ALTER TABLE `t18` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t19`
--

DROP TABLE IF EXISTS `t19`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t19` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1001 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t19`
--

LOCK TABLES `t19` WRITE;
/*!40000 ALTER TABLE `t19` DISABLE KEYS */;
INSERT INTO `t19` VALUES (1000,'诸葛');
/*!40000 ALTER TABLE `t19` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t2`
--

DROP TABLE IF EXISTS `t2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t2` (
  `num` tinyint(3) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t2`
--

LOCK TABLES `t2` WRITE;
/*!40000 ALTER TABLE `t2` DISABLE KEYS */;
INSERT INTO `t2` VALUES (0),(255);
/*!40000 ALTER TABLE `t2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t20`
--

DROP TABLE IF EXISTS `t20`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t20` (
  `id` varchar(25) NOT NULL,
  `number` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `number` (`number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t20`
--

LOCK TABLES `t20` WRITE;
/*!40000 ALTER TABLE `t20` DISABLE KEYS */;
INSERT INTO `t20` VALUES ('321',NULL),('456',NULL),('123456','321');
/*!40000 ALTER TABLE `t20` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t3`
--

DROP TABLE IF EXISTS `t3`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t3` (
  `id` int(11) DEFAULT NULL,
  `a` bit(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t3`
--

LOCK TABLES `t3` WRITE;
/*!40000 ALTER TABLE `t3` DISABLE KEYS */;
INSERT INTO `t3` VALUES (10,_binary '\n'),(10,_binary 'd'),(10,_binary 'a');
/*!40000 ALTER TABLE `t3` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t4`
--

DROP TABLE IF EXISTS `t4`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t4` (
  `name` varchar(20) DEFAULT NULL,
  `gender` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t4`
--

LOCK TABLES `t4` WRITE;
/*!40000 ALTER TABLE `t4` DISABLE KEYS */;
INSERT INTO `t4` VALUES ('马云',_binary ''),('莉莉',_binary '\0');
/*!40000 ALTER TABLE `t4` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t5`
--

DROP TABLE IF EXISTS `t5`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t5` (
  `id` int(11) DEFAULT NULL,
  `salary` float(4,2) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t5`
--

LOCK TABLES `t5` WRITE;
/*!40000 ALTER TABLE `t5` DISABLE KEYS */;
INSERT INTO `t5` VALUES (1,99.99),(1,0.00),(1,0.00);
/*!40000 ALTER TABLE `t5` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t6`
--

DROP TABLE IF EXISTS `t6`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t6` (
  `id` int(11) DEFAULT NULL,
  `salary` float(10,8) DEFAULT NULL,
  `salary2` decimal(10,8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t6`
--

LOCK TABLES `t6` WRITE;
/*!40000 ALTER TABLE `t6` DISABLE KEYS */;
INSERT INTO `t6` VALUES (1,23.12345695,23.12345612);
/*!40000 ALTER TABLE `t6` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t7`
--

DROP TABLE IF EXISTS `t7`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t7` (
  `id` int(11) DEFAULT NULL,
  `name` char(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t7`
--

LOCK TABLES `t7` WRITE;
/*!40000 ALTER TABLE `t7` DISABLE KEYS */;
INSERT INTO `t7` VALUES (1,'ab'),(1,''),(1,'a'),(1,'中国'),(1,'中国');
/*!40000 ALTER TABLE `t7` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t9`
--

DROP TABLE IF EXISTS `t9`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t9` (
  `id` int(11) DEFAULT NULL,
  `name` varchar(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t9`
--

LOCK TABLES `t9` WRITE;
/*!40000 ALTER TABLE `t9` DISABLE KEYS */;
INSERT INTO `t9` VALUES (1,'a'),(1,'ab'),(1,'abc'),(1,'abc1'),(1,'abc1'),(1,'abcdef'),(1,'');
/*!40000 ALTER TABLE `t9` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `votes`
--

DROP TABLE IF EXISTS `votes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `votes` (
  `name` varchar(48) DEFAULT NULL,
  `gender` enum('男','女') DEFAULT NULL,
  `hobby` set('代码','游泳','登山','打篮球','rap') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `votes`
--

LOCK TABLES `votes` WRITE;
/*!40000 ALTER TABLE `votes` DISABLE KEYS */;
INSERT INTO `votes` VALUES ('张三','男','代码'),('张同学','男','代码,登山,rap'),('花木兰','女','登山,打篮球'),('花木兰','女','登山,打篮球'),('花木兰','女','代码'),('花木兰','女','游泳'),('花木兰','女','代码,游泳'),('花木兰','女','登山'),('花木兰','女','代码,游泳,登山,打篮球,rap');
/*!40000 ALTER TABLE `votes` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-10-16 23:11:15
