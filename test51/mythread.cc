#include <iostream>
#include <thread>
#include <pthread.h>
#include <string>
#include <mutex>
#include <cerrno>
#include <cstring>
#include <cstdio>
#include <unistd.h>
using namespace std;

int tickets = 1000;

// void* getTickets(void* args)
// {
// (void)args;
// while(true)
// {
// if(tickets > 0)
// {
// usleep(1000);
// printf("%p: %d\n", pthread_self(), tickets);
// tickets--;
// }
// else
// {
// break;
// }
// }
// return nullptr;
// }
//
// int main()
// {
// pthread_t t1, t2, t3;
// pthread_create(&t1, nullptr, getTickets, nullptr);
// pthread_create(&t1, nullptr, getTickets, nullptr);
// pthread_create(&t1, nullptr, getTickets, nullptr);
//
// pthread_join(t1, nullptr);
// pthread_join(t2, nullptr);
// pthread_join(t3, nullptr);
//
// return 0;
// }

// void func()
// {
// while(true)
// {
// cout << "hello new thread" << endl;
// sleep(1);
// }
// }
//
// int main()
// {
// thread t(func);
//
// while(true)
// {
// cout << "This is main thread" << endl;
// sleep(1);
// }
//
// t.join();
// return 0;
// }
//
// __thread int g_val = 10; //__thread修饰全局变量:线程的局部存储，让每一个线程各自拥有一个全局变量

void *threadRoutine(void *args)
{
    pthread_detach(pthread_self()); // 线程分离

    // sleep(5);
    // execl("/bin/ls", "ls", nullptr);
    // while (true)
    // {
        // cout << (char *)args << " : " << g_val << " 地址: " << &g_val << endl;
        // g_val++;
        // sleep(1);
        // int a = 10;
        // a /= 0;
        // break;
    // }
    // pthread_exit((void *)11);
    cout << "新线程退出了，并且自己释放了资源" << endl;
    return nullptr;
}

int main()
{
    pthread_t tid; // 本质上是一个地址，各自线程的独立属性（独立的栈、id等）
    pthread_create(&tid, nullptr, threadRoutine, (void *)"thread 1");
    while (true)
    {
        // cout << "main thread: " << g_val << " 地址: " << &g_val << endl;
        sleep(1);
        break;
    }
    // pthread_join(tid, nullptr);
    int n = pthread_join(tid, nullptr);
    cout << "n: " << n << " strerr: " << strerror(n) << endl;//因为线程自己释放了资源，所以pthread_join会失败
    return 0;
}
//
// void *threadRoutine(void *args)
// {
// int i = 0;
// int *data = new int[10];
// while (true)
// {
// cout << "这是新线程: " << (char *)args << " running..." << pthread_self() << endl;
// sleep(1);
// data[i] = i;
// if (i++ == 3)
// break;
// int a = 100;
// a /= 0;
// }
// pthread_exit((void *)10);//终止线程的
// exit(10);//终止进程的
// cout << "新线程退出..." << endl;
// return (void*)10;
// return (void*)data;
// }

// int main()
// {
// pthread_t tid;
// pthread_create(&tid, nullptr, threadRoutine, (void *)"thread 1");
//
// int count = 0;
// while(true)
// {
// cout << "这是主线程" << pthread_self() << endl;
// count++;
// if(count > 5) break;
// sleep(1);
// }
//
// pthread_cancel(tid);//线程被取消，退出结果为-1
//
// cout << "tid: " << tid << endl;

// int *ret = nullptr;
// pthread_join(tid, (void **)&ret);
// cout << "main thread wait done ... mian quit ... new thread quit: " << (long long)ret << endl;

// sleep(5);

// for(int i = 0; i < 10; i++)
// {
// cout << ret[i] << " ";
// }
// cout << endl;
// cout << sizeof(void*) << endl;
// while(true)
// {
// cout << "这是主线程" << " running..." << endl;
// sleep(1);
// }
// return 0;
// }

// void *threadRun(void *args)
// {
// const string name = (char *)args;
// while (true)
// {
// cout << name << ", pid: " << getpid() << endl;
// sleep(1);
// }
// }
//
// int main()
// {
// pthread_t tid[5];
// char name[64];
// for (int i = 0; i < 5; i++)
// {
// snprintf(name, sizeof name, "%s-%d", "thread", i + 1);
// pthread_create(tid + i, nullptr, threadRun, (void *)name);
// sleep(1); // 缓解传参的bug问题
// }
//
// while (true)
// {
// cout << "main thread, pid: " << getpid() << endl;
// sleep(3);
// }
// return 0;
// }

// void *threadRoutine(void *args)
// {
// cout << "pthread_self:" << pthread_self() << endl;
// int i = 0;
// int *data = new int[10];
// while (true)
// {
// cout << "这是新线程: " << (char *)args << " running..." << endl;
// sleep(1);
// data[i] = i;
// if (i++ == 5)
// break;
// int a = 10;
// a /= 0;
// }
// cout << "新线程退出..." << endl;
// PTHREAD_CANCELED;
// return (void *)data; // 线程退出的返回值
// pthread_exit((void*)10);
// return nullptr;
// }

// int main()
// {
// pthread_t tid;
// pthread_create(&tid, nullptr, threadRoutine, (void *)"thread 1");
// cout << "tid:" << tid << endl;
//
// void *ret = nullptr;
// pthread_join(tid, &ret);
// cout << "main thread wait done ... mian quit ... new thread quit:" << (long long)ret << endl;

// for (int i = 0; i < 10; i++)
// {
// cout << ret[i] << " ";
// }
// cout << endl;
// return 0;
// }